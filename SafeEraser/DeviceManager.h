#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <list>

#include "utils.h"
#include "PhysicalDevice.h"

using namespace std;

class CDeviceManager
{
protected:
	list<CPhysicalDevice*> physicalDevices;
public:
	CDeviceManager();

	void printPhysicalDevices();
	
	/* Returns string size */
	int getPhysicalDevicesListString(WCHAR *buffer, int bufferLen);

	/* Get a list of partition like //./PhysicalDrive0/1 - meaning partition 1 of PhysicalDrive */
	int getPartitionList(WCHAR *buffer, int bufferLen);

	CPhysicalDevice* getDevicePointer(WCHAR* name);
	CPhysicalDevice* getDevicePointer(int nr);

	~CDeviceManager();
private:
	BOOL getPhysicalDevices();
};

