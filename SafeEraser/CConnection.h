#pragma once


#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <ws2tcpip.h>


class CConnection
{
	SOCKADDR_IN m_addr;
	SOCKET m_sListen;
	SOCKET newConnectionSocket;
public:

	CConnection(unsigned short int port, bool loopBacktoLocalHost);
	~CConnection();

	bool send(unsigned char* buffer, int len);
	bool receive(unsigned char* buffer, int len);

private:
	CConnection(const CConnection& stream);

};
