#include "CConnection.h"
#include "utils.h"

CConnection::CConnection(unsigned short int port, bool loopBacktoLocalHost) {
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);
	if (WSAStartup(DllVersion, &wsaData) != 0)
	{
		MessageBoxA(0, "WinSock startup failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (loopBacktoLocalHost)
		inet_pton(AF_INET, "127.0.0.1", &m_addr.sin_addr.s_addr);
	else
		m_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	m_addr.sin_port = htons(port); //Port
	m_addr.sin_family = AF_INET; //IPv4 Socket

	m_sListen = socket(AF_INET, SOCK_STREAM, 0);
	if (::bind(m_sListen, (SOCKADDR*)&m_addr, sizeof(m_addr)) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to bind the address to our listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (::listen(m_sListen, SOMAXCONN) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to listen on listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}

	int addrlen = sizeof(m_addr);
	newConnectionSocket = accept(m_sListen, (SOCKADDR*)&m_addr, &addrlen);

}

CConnection::~CConnection()
{
	closesocket(m_sListen);
	closesocket(newConnectionSocket);
}

bool CConnection::send(unsigned char* buffer, int len)
{
	int cat = 0;
	int written = 0;

	while (len > 0)
	{
		cat = ::send(newConnectionSocket, (const char *)(buffer + written), len, 0);
		if (cat < 0)
			return false;
		written += 0;
		len -= cat;
	}
	return true;
}

bool CConnection::receive(unsigned char* buffer, int len)
{
	int cat = 0;
	int read = 0;

	while (len > 0)
	{
		cat = ::recv(newConnectionSocket, (char *)(buffer + read), len, 0);
		if (cat < 0)
			return false;
		read += cat;
		len -= cat;
	}
	return true;
}

