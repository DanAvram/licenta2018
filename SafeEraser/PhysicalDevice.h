#pragma once
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include <SetupAPI.h>
#include "OverwritePattern.h"
#include "Erasable.h"

class CPartition;

using namespace std;
class CPhysicalDevice : public IErasable
{
	wstring name;
	wstring path;
	DWORD diskNumer;
	LARGE_INTEGER size;
	HANDLE hDevice;

	/* Information about partitions */
	DRIVE_LAYOUT_INFORMATION_EX *partBuffer; 
	
public:
	/* Move to private */
	DISK_GEOMETRY diskGeometry;

	DWORD write(LARGE_INTEGER offset, unsigned char* buffer, DWORD lenght);
	CPhysicalDevice(LPWSTR name);
	CPhysicalDevice(LPWSTR name, LPWSTR path, DWORD diskNumber);

	/* Safe erase PhysicalDevice*/
	bool erase(OverwritePattern pattern);
	bool forceErase(OverwritePattern pattern);


	BOOL read_sector(unsigned char **buffer, ULONGLONG sector_nr);
	
	inline DWORD getDiskNumber() { return diskNumer; }
	inline const DRIVE_LAYOUT_INFORMATION_EX *getPartitionsBuffer() { return partBuffer; }
	wstring getName();
	wstring getPath();
	LONGLONG getDiskSizeInBytes();

	CPartition *getPartition(int nr);

	~CPhysicalDevice();
private:
	BOOL getDiskGeometry();
	void getPartionLayout();
	BOOL CPhysicalDevice::unmountMountedVolumes();
};

