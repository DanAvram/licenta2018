#pragma once
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include "Erasable.h"
#include "NTFSParseLib/NTFS.h"

using namespace std;


class CFileObject : public IErasable
{
	wstring path;
	int physicalDevice;
	CNTFSVolume *volume;
	CFileRecord *fileRecord;

	CFileObject(const CFileObject &obj) {}
private:
	static void eraseCallback(const CIndexEntry *ie) {
		// Hide system metafiles
		if (ie->GetFileReference() < MFT_IDX_USER)
			return;

		// Ignore DOS alias file names
		if (!ie->IsWin32Name())
			return;

		char fn[MAX_PATH];
		int fnlen = ie->GetFileName(fn, MAX_PATH);
		if (fnlen > 0){
			printf("FILE: %s\n", fn);

		}
	}
public:
	/* Obsolete */
	int getPhysicalOffsetOnDisk(LARGE_INTEGER *res);

	/* Override */
	virtual bool erase(OverwritePattern patternGen);

	virtual bool forceErase(OverwritePattern patternGen);

	void eraseSubentries()
	{
		this->fileRecord->TraverseSubEntries(eraseCallback);
	}

	CFileObject(wstring);
	~CFileObject();

};

