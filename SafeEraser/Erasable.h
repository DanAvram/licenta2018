#pragma once
#include "OverwritePattern.h"

/* Interface for erasable objects: i.e. files, partitions*/
class IErasable
{
public:
	virtual bool erase(OverwritePattern pattern) = 0;
	virtual bool forceErase(OverwritePattern pattern) = 0;
	virtual ~IErasable() {};
};