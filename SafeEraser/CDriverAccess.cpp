/* GNU General Public License version 3 */
/* https://www.codeproject.com/Articles/28314/Reading-and-Writing-to-Raw-Disk-Sectors */
/* Modified 06/06/2018*/

#include "CDriverAccess.h"

BOOL CDriverAccess::getLoadDriverPriv()
{
	HANDLE hToken;
	LUID huid;
	LUID_AND_ATTRIBUTES priv;
	TOKEN_PRIVILEGES tp;

	if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES, &hToken))
	{

		if (LookupPrivilegeValueA(NULL, "SeLoadDriverPrivilege", &huid))
		{
			priv.Attributes = SE_PRIVILEGE_ENABLED;
			priv.Luid = huid;

			tp.PrivilegeCount = 1;
			tp.Privileges[0] = priv;

			if (AdjustTokenPrivileges(hToken, FALSE, &tp, 0, NULL, NULL))
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL CDriverAccess::setupRegistry()
{
	HKEY hkey;
	DWORD val;
	const char *imgName = "System32\\DRIVERS\\" DRV_FILENAME;

	if (RegCreateKeyA(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\" DRV_NAME, &hkey) != ERROR_SUCCESS)
		return FALSE;


	val = 1;
	if (RegSetValueExA(hkey, "Type", 0, REG_DWORD, (PBYTE)&val, sizeof(val)) != ERROR_SUCCESS)
		return FALSE;

	if (RegSetValueExA(hkey, "ErrorControl", 0, REG_DWORD, (PBYTE)&val, sizeof(val)) != ERROR_SUCCESS)
		return FALSE;

	val = 3;
	if (RegSetValueExA(hkey, "Start", 0, REG_DWORD, (PBYTE)&val, sizeof(val)) != ERROR_SUCCESS)
		return FALSE;


	if (RegSetValueExA(hkey, "ImagePath", 0, REG_EXPAND_SZ, (PBYTE)imgName, strlen(imgName)) != ERROR_SUCCESS)
		return FALSE;

	return TRUE;
}

BOOL CDriverAccess::loadDriver()
{
	// call ntdll APIs
	HMODULE hntdll;
	ANSI_STRING aStr;
	UNICODE_STRING uStr;

	NTSTATUS(WINAPI * _RtlAnsiStringToUnicodeString)
		(PUNICODE_STRING  DestinationString,
			IN PANSI_STRING  SourceString,
			IN BOOLEAN);

	VOID(WINAPI *_RtlInitAnsiString)
		(IN OUT PANSI_STRING  DestinationString,
			IN PCHAR  SourceString);

	NTSTATUS(WINAPI * _ZwLoadDriver)
		(IN PUNICODE_STRING DriverServiceName);

	NTSTATUS(WINAPI * _ZwUnloadDriver)
		(IN PUNICODE_STRING DriverServiceName);

	VOID(WINAPI * _RtlFreeUnicodeString)
		(IN PUNICODE_STRING  UnicodeString);


	hntdll = GetModuleHandleA("ntdll.dll");

	*(FARPROC *)&_ZwLoadDriver = GetProcAddress(hntdll, "NtLoadDriver");

	*(FARPROC *)&_ZwUnloadDriver = GetProcAddress(hntdll, "NtUnloadDriver");

	*(FARPROC *)&_RtlAnsiStringToUnicodeString =
		GetProcAddress(hntdll, "RtlAnsiStringToUnicodeString");

	*(FARPROC *)&_RtlInitAnsiString =
		GetProcAddress(hntdll, "RtlInitAnsiString");

	*(FARPROC *)&_RtlFreeUnicodeString =
		GetProcAddress(hntdll, "RtlFreeUnicodeString");

	if (_ZwLoadDriver && _ZwUnloadDriver && _RtlAnsiStringToUnicodeString &&
		_RtlInitAnsiString && _RtlFreeUnicodeString)
	{
		_RtlInitAnsiString(&aStr,
			(PCHAR) "\\Registry\\Machine\\System\\CurrentControlSet\\Services\\" DRV_NAME);

		if (_RtlAnsiStringToUnicodeString(&uStr, &aStr, TRUE) != STATUS_SUCCESS)
			return FALSE;
		else
		{
			if (_ZwLoadDriver(&uStr) == STATUS_SUCCESS)
			{
				_RtlFreeUnicodeString(&uStr);
				return TRUE;
			}
			_RtlFreeUnicodeString(&uStr);
		}
	}

	return FALSE;
}

void CDriverAccess::cleanupDriver(void)
{
	char sysDir[MAX_PATH + 1];
	GetSystemDirectoryA(sysDir, MAX_PATH);
	strncat(sysDir, "\\drivers\\" DRV_FILENAME, MAX_PATH);
	DeleteFileA(sysDir);

	RegDeleteKeyA(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\" DRV_NAME "\\Enum");
	RegDeleteKeyA(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\" DRV_NAME);
}

BOOL CDriverAccess::unloadDriver()
{
	// call ntdll APIs
	HMODULE hntdll;
	ANSI_STRING aStr;
	UNICODE_STRING uStr;

	NTSTATUS(WINAPI * _RtlAnsiStringToUnicodeString)
		(PUNICODE_STRING  DestinationString,
			IN PANSI_STRING  SourceString,
			IN BOOLEAN);

	VOID(WINAPI *_RtlInitAnsiString)
		(IN OUT PANSI_STRING  DestinationString,
			IN PCHAR  SourceString);

	NTSTATUS(WINAPI * _ZwLoadDriver)
		(IN PUNICODE_STRING DriverServiceName);

	NTSTATUS(WINAPI * _ZwUnloadDriver)
		(IN PUNICODE_STRING DriverServiceName);

	VOID(WINAPI * _RtlFreeUnicodeString)
		(IN PUNICODE_STRING  UnicodeString);


	hntdll = GetModuleHandleA("ntdll.dll");

	*(FARPROC *)&_ZwLoadDriver = GetProcAddress(hntdll, "NtLoadDriver");

	*(FARPROC *)&_ZwUnloadDriver = GetProcAddress(hntdll, "NtUnloadDriver");

	*(FARPROC *)&_RtlAnsiStringToUnicodeString =
		GetProcAddress(hntdll, "RtlAnsiStringToUnicodeString");

	*(FARPROC *)&_RtlInitAnsiString =
		GetProcAddress(hntdll, "RtlInitAnsiString");

	*(FARPROC *)&_RtlFreeUnicodeString =
		GetProcAddress(hntdll, "RtlFreeUnicodeString");

	if (_ZwLoadDriver && _ZwUnloadDriver && _RtlAnsiStringToUnicodeString &&
		_RtlInitAnsiString && _RtlFreeUnicodeString)
	{

		_RtlInitAnsiString(&aStr,
			(PCHAR)"\\Registry\\Machine\\System\\CurrentControlSet\\Services\\" DRV_NAME);

		if (_RtlAnsiStringToUnicodeString(&uStr, &aStr, TRUE) != STATUS_SUCCESS)
			return FALSE;
		else
		{
			if (_ZwUnloadDriver(&uStr) == STATUS_SUCCESS)
			{
				_RtlFreeUnicodeString(&uStr);
				return TRUE;
			}
			_RtlFreeUnicodeString(&uStr);
		}
	}

	return FALSE;
}


void CDriverAccess::uninstallDriver(void)
{
	char drvFullPath[MAX_PATH + 1];
	char *filePart;
	HANDLE hFile;
	char sysDir[MAX_PATH + 1];

	ZeroMemory(drvFullPath, MAX_PATH);
	GetFullPathNameA(DRV_FILENAME, MAX_PATH, drvFullPath, &filePart);

	hFile = CreateFileA(drvFullPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL, 0);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Cannot find required driver file %s\n", drvFullPath);
		return;
	}
	else
	{
		CloseHandle(hFile);

		GetSystemDirectoryA(sysDir, MAX_PATH);
		strncat(sysDir, "\\drivers\\" DRV_FILENAME, MAX_PATH);
		CopyFileA(drvFullPath, sysDir, TRUE);

		if (!getLoadDriverPriv())
		{
			printf("Error getting load driver privilege!\n");
		}
		else
		{
			if (!setupRegistry())
			{
				printf("Error setting driver registry keys!\nMake sure you are running this as Administrator.\n");
			}
			else
			{
				if (unloadDriver())
					printf("Support driver successfully unloaded.\n");
				else
					printf("Unload support driver failed.  It is probably not loaded.\n");
			}
		}
		cleanupDriver();
	}
}

HANDLE CDriverAccess::openDriver(void)
{
	HANDLE hDevice;
	HANDLE hFile;
	char drvFullPath[MAX_PATH + 1];
	char *filePart;
	char sysDir[MAX_PATH + 1];

	hDevice = CreateFileA("\\\\.\\" DRV_NAME, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_EXISTING, FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, NULL);

	if (hDevice == INVALID_HANDLE_VALUE)
	{


		ZeroMemory(drvFullPath, MAX_PATH);
		GetFullPathNameA(DRV_FILENAME, MAX_PATH, drvFullPath, &filePart);

		hFile = CreateFileA(drvFullPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL, 0);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			printf("Cannot find required driver file %s\n", drvFullPath);
			return INVALID_HANDLE_VALUE;
		}
		else
		{
			CloseHandle(hFile);

			GetSystemDirectoryA(sysDir, MAX_PATH);
			strncat(sysDir, "\\drivers\\" DRV_FILENAME, MAX_PATH);
			CopyFileA(drvFullPath, sysDir, TRUE);

			if (!getLoadDriverPriv())
			{
				printf("Error getting load driver privilege!\n");
			}
			else
			{
				if (!setupRegistry())
				{
					printf("Error setting driver registry keys!\nMake sure you are running this as Administrator.\n");
				}
				else
				{
					loadDriver();
					hDevice = CreateFileA("\\\\.\\" DRV_NAME, GENERIC_WRITE | GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
						NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
					if (hDevice == INVALID_HANDLE_VALUE)
					{
						printf("Error loading kernel support driver!\nMake sure you are running this as Administrator.\n");
					}
				}
			}
			cleanupDriver();
		}
	}

	return hDevice;
}

BOOL CDriverAccess::read_sector(unsigned char *buffer, unsigned int device_number, LARGE_INTEGER sector_no)
{

	HANDLE hDevice = openDriver();
	if (hDevice == INVALID_HANDLE_VALUE) {
		LOG_ERROR(L"%s", L"Can't Open Driver\n");
		return FALSE;
	}

	DWORD returnSize;
	DISK_LOCATION dlInfo;

	dlInfo.bIsRawDiskObj = TRUE;
	dlInfo.dwDiskOrdinal = device_number;;
	dlInfo.ullSectorNum = sector_no.QuadPart;

	DeviceIoControl(hDevice, IOCTL_SECTOR_READ, &dlInfo, sizeof(DISK_LOCATION), buffer, 512, &returnSize, NULL);

	// TODO: ERROR CHECK
	CloseHandle(hDevice);
}

BOOL CDriverAccess::write_sector(unsigned char *buffer, unsigned int device_number, LARGE_INTEGER sector_no)
{
	HANDLE hDevice = openDriver();
	if (hDevice == INVALID_HANDLE_VALUE) {
		LOG_ERROR(L"%s", L"Can't Open Driver\n");
		return FALSE;
	}

	DWORD writeBufferSize = sizeof(DISK_LOCATION) + 512;
	DWORD returnSize;
	unsigned char * writeBuffer = (unsigned char *)malloc(writeBufferSize);

	DISK_LOCATION dlInfo;

	dlInfo.bIsRawDiskObj = TRUE;
	dlInfo.dwDiskOrdinal = device_number;;
	dlInfo.ullSectorNum = sector_no.QuadPart;

	memcpy(writeBuffer, (void *)&dlInfo, sizeof(DISK_LOCATION));
	memcpy(writeBuffer + sizeof(DISK_LOCATION), buffer, 512);

	BOOL ret = DeviceIoControl(hDevice, IOCTL_SECTOR_WRITE, writeBuffer, writeBufferSize, NULL, 0, &returnSize, NULL);
	if (ret == FALSE) {
		LOG_ERROR(L"%s", ShowError());
		return FALSE;
	}

	free(writeBuffer);
	CloseHandle(hDevice);

	return TRUE;
}


