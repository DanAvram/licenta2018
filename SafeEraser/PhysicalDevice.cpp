#include "PhysicalDevice.h"
#include "CDriverAccess.h"
#include "CPartition.h"


CPhysicalDevice::CPhysicalDevice(LPWSTR name)
{
	this->name = wstring(name);
	this->diskNumer = this->name.c_str()[17] - '0';

	hDevice = CreateFileW(this->name.c_str(),
		0,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,    // disposition
		0,                // file attributes
		NULL);

	getDiskGeometry();
	getPartionLayout();
	size.QuadPart = diskGeometry.TracksPerCylinder*diskGeometry.SectorsPerTrack*diskGeometry.Cylinders.QuadPart *diskGeometry.BytesPerSector;

	         // do not copy file attributes
}

void CPhysicalDevice::getPartionLayout()
{
	/* Buffer for 16 entries - should be enough */
	DWORD bufferSize = sizeof(DRIVE_LAYOUT_INFORMATION_EX) + 16 * sizeof(PARTITION_INFORMATION_EX);
	partBuffer = (DRIVE_LAYOUT_INFORMATION_EX *)malloc(bufferSize);

	if (!DeviceIoControl(hDevice,  // handle to device
		IOCTL_DISK_GET_DRIVE_LAYOUT_EX, // dwIoControlCode
		NULL,                           // lpInBuffer
		0,                              // nInBufferSize
		partBuffer,			    	    // output buffer
		bufferSize,						// size of output buffer
		NULL,							// number of bytes returned
		NULL))							// OVERLAPPED structure
	{
		LOG_ERROR(L"%s", ShowError());
		return;
	}
}

CPartition *CPhysicalDevice::getPartition(int nr)
{
	if (nr >= 0 && nr < partBuffer->PartitionCount)
	{
		return new CPartition(this, partBuffer->PartitionEntry[nr]);
	}
}



CPhysicalDevice::CPhysicalDevice(LPWSTR name, LPWSTR path, DWORD diskNumber)
{
	this->name = wstring(name);
	this->path = wstring(path);
	this->diskNumer = diskNumber;

	hDevice = CreateFileW(this->name.c_str(),
		0,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,    // disposition
		0,                // file attributes
		NULL);

	getDiskGeometry();
	size.QuadPart = diskGeometry.TracksPerCylinder*diskGeometry.SectorsPerTrack*diskGeometry.Cylinders.QuadPart *diskGeometry.BytesPerSector;
	getPartionLayout();
}

BOOL CPhysicalDevice::unmountMountedVolumes()
{
	DWORD logicalDriveStringsLength;
	TCHAR logicalDriveBuffer[512];

	DWORD volumeExtentsBufferSize = sizeof(VOLUME_DISK_EXTENTS) + (4 * sizeof(DISK_EXTENT));
	PVOLUME_DISK_EXTENTS volumeExtentBuffer = (VOLUME_DISK_EXTENTS*)malloc(volumeExtentsBufferSize);

	logicalDriveStringsLength = GetLogicalDriveStrings(
		512,
		logicalDriveBuffer
	);
	for (int i = 0; i < logicalDriveStringsLength; i += 4)
	{
		TCHAR currentDriveLetter;
		TCHAR drivePath[8];

		currentDriveLetter = logicalDriveBuffer[i];
		wsprintf(drivePath, L"\\\\.\\%c:", currentDriveLetter);

		HANDLE CHandle = CreateFile(
			drivePath,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);

		if (!DeviceIoControl((HANDLE)CHandle,     // handle to device
			IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS, // dwIoControlCode
			NULL,                                 // lpInBuffer
			0,                                    // nInBufferSize
			volumeExtentBuffer,                   // output buffer
			volumeExtentsBufferSize,              // size of output buffer
			NULL,							   	  // number of bytes returned
			NULL								  // OVERLAPPED structure
		))
		{
			CloseHandle(CHandle);
			LOG_ERROR("%s", ShowError());
			return FALSE;
		}
		CloseHandle(CHandle);

		/* Check if volume is on current partition */


	
		if (volumeExtentBuffer->NumberOfDiskExtents <= 1) {
			if (volumeExtentBuffer->Extents[0].DiskNumber == this->getDiskNumber())
			{
				TCHAR volumes[10];
				wsprintf(volumes, L"\\\\.\\%c:", currentDriveLetter);
				HANDLE CHandle = CreateFile(
					volumes,
					GENERIC_READ | GENERIC_WRITE,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL);

				if (CHandle == INVALID_HANDLE_VALUE)
				{
					LOG_ERROR(L"%s", ShowError());
					LOG_ERROR(L"%s", L"Volume couldn't be open");
					return FALSE;
				}

				if (!DeviceIoControl(
					(HANDLE)CHandle,             // handle to a volume
					(DWORD)FSCTL_LOCK_VOLUME,    // dwIoControlCode
					NULL,                        // lpInBuffer
					0,                           // nInBufferSize
					NULL,                        // lpOutBuffer
					0,                           // nOutBufferSize
					NULL,						 // number of bytes returned
					NULL						 // OVERLAPPED structure
				))
				{
					LOG_ERROR(L"%s", ShowError());
					return FALSE;
				}

				if (!DeviceIoControl(
					(HANDLE)CHandle,             // handle to a volume
					(DWORD)FSCTL_DISMOUNT_VOLUME,    // dwIoControlCode
					NULL,                        // lpInBuffer
					0,                           // nInBufferSize
					NULL,                        // lpOutBuffer
					0,                           // nOutBufferSize
					NULL,						 // number of bytes returned
					NULL						 // OVERLAPPED structure
				))
				{
					LOG_ERROR(L"%s", ShowError());
					return FALSE;
				}
				
			}
			else {
				continue;
			}
		}
		else {
			LOG_ERROR("%s", "NUMBER OF EXTENTS > 1, NOT SUPPORTED YET!");
			return FALSE;
		}
	}
	return TRUE;
}

bool CPhysicalDevice::erase(OverwritePattern pattern)
{
	BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);
	do
	{
		LARGE_INTEGER offset;
		LARGE_INTEGER lenght = size;
		DWORD bytesWritten;
		unsigned char buffer[512];

		while (lenght.QuadPart > 0)
		{
			patternGen->getBuffer(buffer, 512);
			bytesWritten = write(offset, buffer, 512);
			if (bytesWritten == 0)
			{
				LOG_ERROR(L"%s", ShowError());
				return false;
			}
			lenght.QuadPart -= 512;
		}
	} while (patternGen->nextPattern() != -1);
	delete patternGen;
	return true;
}

bool CPhysicalDevice::forceErase(OverwritePattern pattern)
{
	unmountMountedVolumes();
	BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);
	do
	{
		LARGE_INTEGER offset;
		offset.QuadPart = 0;
		LARGE_INTEGER lenght = size;
		DWORD bytesWritten;
		unsigned char buffer[512];

		while (lenght.QuadPart > 0)
		{
			patternGen->getBuffer(buffer, 512);
			CDriverAccess::write_sector(buffer, getDiskNumber(), offset);
			offset.QuadPart++;
			lenght.QuadPart -= 512;
		}
	} while (patternGen->nextPattern() != -1);
	delete patternGen;
	return true;
}


/* TODO: Optimize OpenHandle - Close handle by keeping a reference */
DWORD CPhysicalDevice::write(LARGE_INTEGER offset, unsigned char* buffer, DWORD lenght)
{

	SetFilePointerEx(hDevice, offset, NULL, FILE_BEGIN);

	DWORD bytesWritten;

	WriteFile(hDevice, buffer, lenght, &bytesWritten, NULL);

	return bytesWritten;

}

BOOL CPhysicalDevice::read_sector(unsigned char **buffer, ULONGLONG sector_nr)
{
	HANDLE devHandle = CreateFile(name.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	LARGE_INTEGER LIoffset;
	LIoffset.QuadPart = sector_nr * 512;
	/* TODO: change 512 to diskGeometry...*/
	SetFilePointerEx(devHandle, LIoffset, NULL, FILE_BEGIN);

	DWORD bytesRead;
	if (*buffer == NULL)
	{
		*buffer = (unsigned char*)malloc(diskGeometry.BytesPerSector);	
	}

	ReadFile(devHandle, *buffer, diskGeometry.BytesPerSector, &bytesRead, NULL);
	CloseHandle(devHandle);

	return TRUE;
}

wstring CPhysicalDevice::getName()
{
	return name;
}

wstring CPhysicalDevice::getPath()
{
	return path;
}

LONGLONG CPhysicalDevice::getDiskSizeInBytes()
{
	return diskGeometry.BytesPerSector * diskGeometry.SectorsPerTrack * 
		diskGeometry.TracksPerCylinder * diskGeometry.Cylinders.QuadPart;
}


BOOL CPhysicalDevice::getDiskGeometry()
{
	BOOL bResult = FALSE;
	DWORD junk = 0;

	bResult = DeviceIoControl(hDevice,					// device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY,					// operation to perform
		NULL, 0,										// no input buffer
		&diskGeometry, sizeof(diskGeometry),			// output buffer
		&junk,											// # bytes returned
		(LPOVERLAPPED)NULL);							// synchronous I/O

	return (bResult);
}



CPhysicalDevice::~CPhysicalDevice()
{
	free(partBuffer);
}