#include "DeviceManager.h"
#include <iostream>
#include <winioctl.h>
#include <SetupAPI.h>

CDeviceManager::CDeviceManager()
{
	getPhysicalDevices();
}

BOOL CDeviceManager::getPhysicalDevices()
{
	HDEVINFO diskDevices;
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInterfaceDetailData;
	DWORD devNumber = 0;
	DWORD size;
	BOOL ret = FALSE;

	STORAGE_DEVICE_NUMBER diskNumber;
	DWORD bytesReturned;

	diskDevices = SetupDiGetClassDevs(
		&GUID_DEVINTERFACE_DISK,
		NULL,
		NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE
	);

	if (diskDevices == INVALID_HANDLE_VALUE) {
		LOG_ERROR("SetupDietClassDevs failed!");
		return false;
	}

	ZeroMemory(&deviceInterfaceData, sizeof(SP_DEVICE_INTERFACE_DATA));
	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	while (SetupDiEnumDeviceInterfaces(
		diskDevices,
		NULL,
		&GUID_DEVINTERFACE_DISK,
		devNumber,
		&deviceInterfaceData)) {

		devNumber++;

		// Returns details about a device interface in second argument
		// First time is called to get the size
		ret = SetupDiGetDeviceInterfaceDetail(diskDevices,
			&deviceInterfaceData,
			NULL,
			0,
			&size,
			NULL);
		if ((ret == FALSE) && (GetLastError() != ERROR_INSUFFICIENT_BUFFER)) {
			LOG_INFO("SetupDiGetDeviceInterfaceDetail failed!");
			continue;
		}

		deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(size);

		ZeroMemory(deviceInterfaceDetailData, size);
		deviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

		// Second time is called for the information
		ret = SetupDiGetDeviceInterfaceDetail(diskDevices,
			&deviceInterfaceData,
			deviceInterfaceDetailData,
			size,
			NULL,	
			NULL);

		if (ret == FALSE) {
			LOG_INFO("SetupDiGetDeviceInterfaceDetail failed!");
			continue;
		}

		HANDLE disk = CreateFile(deviceInterfaceDetailData->DevicePath,
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (INVALID_HANDLE_VALUE == disk) {
			LOG_ERROR("Failed to CreateFile!");
			continue;
		}

		ret = DeviceIoControl(disk,
			IOCTL_STORAGE_GET_DEVICE_NUMBER,
			NULL,
			0,
			&diskNumber,
			sizeof(STORAGE_DEVICE_NUMBER),
			&bytesReturned,
			NULL);
		if (0 == ret) {
			LOG_ERROR("DeviceIoControl failed!");
			continue;
		}
		
		LPWSTR devName = (LPWSTR)malloc(sizeof(LPWSTR) * 50);
		swprintf(devName, L"\\\\.\\PhysicalDrive%d", diskNumber.DeviceNumber);

		physicalDevices.push_back(new CPhysicalDevice(devName, deviceInterfaceDetailData->DevicePath, diskNumber.DeviceNumber));

		free(devName);
	}
	return 0;
}

void CDeviceManager::printPhysicalDevices()
{
	if (physicalDevices.empty() == true) {
		getPhysicalDevices();
	}

	if (physicalDevices.empty() == true) {
		LOG_INFO("No physical devices found!\n");
		return;
	}

	for (CPhysicalDevice* dev : physicalDevices)
	{
		wprintf(L"Name: %s\nPath: %s\nSize: %lld bytes (%ld GB)\n\n", dev->getName().c_str(), dev->getPath().c_str(), dev->getDiskSizeInBytes(), dev->getDiskSizeInBytes() / BYTES_TO_GB);
	}
}

int CDeviceManager::getPhysicalDevicesListString(WCHAR * buffer, int bufferLen)
{
	int strLen = 0;
	for (CPhysicalDevice* dev : physicalDevices)
	{
		int currentSize = wcslen(dev->getName().c_str()) + 1;
		strLen += currentSize;

		if (strLen > bufferLen)
			return -1;

		wcscpy(buffer, dev->getName().c_str());
		buffer += currentSize;
	}
	return strLen;
}

int CDeviceManager::getPartitionList(WCHAR *buffer, int bufferLen)
{
	int strLen = 0;
	for (CPhysicalDevice* dev : physicalDevices)
	{
		int currentSize = 0;

		for (int i = 0; i < dev->getPartitionsBuffer()->PartitionCount; i++)
		{
			wcscpy(buffer, dev->getName().c_str());

			WCHAR devnum[16];
			wsprintf(devnum, L"Partition%d", i);
			
			currentSize = wcslen(dev->getName().c_str()) + 1 + wcslen(devnum) + 1;
			strLen += currentSize;

			if (strLen > bufferLen)
				return -1;

			wcscat(buffer, L"\\");
			wcscat(buffer, devnum);

			buffer += currentSize;
		}
	}
	return strLen;
}


CPhysicalDevice * CDeviceManager::getDevicePointer(WCHAR * name)
{
	for (CPhysicalDevice* dev : physicalDevices)
	{
		if (wcscmp(name, dev->getName().c_str()))
		{
			return dev;
		}
	}
	return NULL;
}

CPhysicalDevice * CDeviceManager::getDevicePointer(int nr)
{
	for (CPhysicalDevice* dev : physicalDevices)
	{
		if (dev->getDiskNumber() == nr)
		{
			return dev;
		}
	}
	return NULL;
}


CDeviceManager::~CDeviceManager()
{
	CPhysicalDevice *dev;

	while (this->physicalDevices.size() > 0)
	{
		dev = physicalDevices.back();
		delete dev;

		physicalDevices.pop_back();
	}
}
