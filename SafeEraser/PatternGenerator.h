#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class IPatternGenerator
{
	virtual void generatePattern(unsigned char* buffer, size_t size) = 0;
};



class CZeroGenerator : public IPatternGenerator
{
	void inline generatePattern(unsigned char* buffer, size_t size)
	{
		if (NULL == buffer)
			buffer = (unsigned char*)malloc(size);

		memset((unsigned char*)buffer, 0, size);
	}
};

