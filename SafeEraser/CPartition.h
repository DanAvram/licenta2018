#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include "Erasable.h"
#include "PhysicalDevice.h"


class CPartition :
	public IErasable
{
	PARTITION_INFORMATION_EX partitionInformation;
	CPhysicalDevice* disk;

	/* Can only be instantiated from a CPhysicalDevice*/
	CPartition(CPhysicalDevice* disk, PARTITION_INFORMATION_EX aPartitionInformation);
	friend class CPhysicalDevice;
public:
	BOOL checkMountedVolumes(LPTSTR mountedVolumes);
	bool erase(OverwritePattern pattern);
	bool forceErase(OverwritePattern pattern);
	~CPartition();

private:
	void unmountPartitionVolumes();
};

