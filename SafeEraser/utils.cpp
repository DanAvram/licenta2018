#include "utils.h"


WCHAR getvolume(WCHAR **ppath)
{
	WCHAR *p = *ppath;
	WCHAR volname;

	// skip leading blank and "
	while (*p)
	{
		if (*p == ' ' || *p == '"')
			p++;
		else
			break;
	}
	if (*p == '\0')
		return '\0';
	else
	{
		volname = *p;
		p++;
	}

	// skip blank
	while (*p)
	{
		if (*p == ' ')
			p++;
		else
			break;
	}
	if (*p == '\0')
		return '\0';

	if (*p != ':')
		return '\0';

	// forward to '\' or string end
	while (*p)
	{
		if (*p != '\\')
			p++;
		else
			break;
	}
	// forward to not '\' and not ", or string end
	while (*p)
	{
		if (*p == '\\' || *p == '"')
			p++;
		else
			break;
	}

	*ppath = p;
	return volname;
}

// get sub directory name
// *ppath -> "program files\common files"
int getpathname(WCHAR **ppath, WCHAR *pathname)
{
	int len = 0;
	WCHAR *p = *ppath;

	// copy until '\' or " or string ends or buffer full
	while (*p && len < MAX_PATH)
	{
		pathname[len] = *p;
		len++;
		p++;

		if (*p == '\\' || *p == '\"')
			break;
	}
	pathname[len] = '\0';

	// forward to not '\' and not ", or string end
	while (*p)
	{
		if (*p == '\\' || *p == '\"')
			p++;
		else
			break;
	}

	*ppath = p;
	return len;
}

