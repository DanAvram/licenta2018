#include <stdio.h>

#include <strsafe.h>
#include "utils.h"

#include "FileObject.h"
#include "DeviceManager.h"
#include "PhysicalDevice.h"

#include "CDriverAccess.h"
#include "CConnection.h"
#include "CPartition.h"

#include "Scheduler.h"

#pragma comment (lib, "Setupapi.lib")
#pragma comment(lib, "Ws2_32.lib")

#include "OpCodes.h"
#include <iostream>


bool folder_erase(WCHAR* path);

int main()
{


#if 1
	CDeviceManager dm;
	CConnection con(13579, true);
	OverwritePattern op;

	Bosma::Scheduler * sched = new Bosma::Scheduler(5);

	char *bufferIn = new char[2048];
	char *bufferOut = new char[2048];

	int inLength = 0;
	int outLength = 0;

	int opCode = 0;

	for (;;)
	{
		/* Get request size */
		con.receive((unsigned char *)bufferIn, 4);
		inLength = *((uint32_t*)bufferIn);

		con.receive((unsigned char *)bufferIn, inLength);

		opCode = *((uint32_t *)bufferIn);
		bufferIn += 4;

		switch (opCode)
		{
		case GET_DISKS:
		{
			outLength = dm.getPhysicalDevicesListString((WCHAR*)bufferOut, 1024) * 2;
			con.send((unsigned char *)&outLength, sizeof(uint32_t));

			con.send((unsigned char *)bufferOut, outLength);

			break;
		}

		case GET_PARTITIONS:
		{
			outLength = dm.getPartitionList((WCHAR*)bufferOut, 1024) * 2;
			con.send((unsigned char *)&outLength, sizeof(uint32_t));

			con.send((unsigned char *)bufferOut, outLength);

			break;
		}

		case ERASE_FILE:
		{
			op = (OverwritePattern) *((uint32_t *)(bufferIn));
			bufferIn += 4;

			CFileObject fo((WCHAR *)(bufferIn));

			fo.forceErase(op);
			wprintf(L"Done deleting %s\n", bufferIn);

			break;
		}

		case ERASE_PARTITIONS:
		{
			op = (OverwritePattern) *((uint32_t *)bufferIn);
			bufferIn += 4;

			int devNr = *((int *)bufferIn);
			bufferIn += 4;

			int partNr = *((int *)bufferIn);

			CPhysicalDevice *dev = dm.getDevicePointer(devNr);
			CPartition *part = dev->getPartition(partNr);


			part->forceErase(op);
			printf("Done deleting partition.\n");
			break;
		}

		case ERASE_DISK:
		{
			op = (OverwritePattern) *((uint32_t *)bufferIn);
			bufferIn += 4;

			int devNr = *((int *)bufferIn);
			bufferIn += 4;

			CPhysicalDevice *dev = dm.getDevicePointer(devNr);

			dev->forceErase(op);


			break;
		}

		case SCHED_TASK_NONREC:
		{
			char* datetime = (char*)malloc(20);

			for (int i = 0; i < 19; i++)
			{
				datetime[i] = bufferIn[i << 1];
			}

			datetime[19] = L'\0';

			bufferIn += 38;

			op = (OverwritePattern) *((uint32_t *)bufferIn);
			bufferIn += 4;


			char *buffer_sched =(char*) wcsdup((WCHAR*)bufferIn);
			sched->at(datetime, [buffer_sched, datetime]() {
				wprintf(L"Deleting %s\n", (WCHAR*)buffer_sched);
				folder_erase((WCHAR*)buffer_sched);
				free(datetime);
				free(buffer_sched);
			});


			break;
		}

		case SCHED_TASK_REC:
		{
			Schedulling rectype;
			int hour;
			int min;

			rectype = (Schedulling)*((uint32_t *)bufferIn);
			bufferIn += 4;

			hour = *((uint32_t *)bufferIn);
			bufferIn += 4;

			min = *((uint32_t *)bufferIn);
			bufferIn += 4;

			op = (OverwritePattern) *((uint32_t *)bufferIn);
			bufferIn += 4;

			char *buffer_sched = (char*)wcsdup((WCHAR*)bufferIn);
			char buffer[20];

			switch (rectype) {
			case Schedulling::SCHED_DAILY:
				sprintf(buffer, "%d %d * * *", min, hour);
				break;
			case Schedulling::SCHED_MONTHLY:
				sprintf(buffer, "%d %d * 1 *", min, hour);
				break;
			case Schedulling::SCHED_WEEKLY:
				sprintf(buffer, "%d %d * * 1", min, hour);
				break;
			default:
				printf("Error: incorect schedulling parameters!");
			}


			sched->cron(buffer, [buffer_sched]() {
				wprintf(L"Deleting %s\n", (WCHAR*)buffer_sched);
				folder_erase((WCHAR*)buffer_sched);
				free(buffer_sched);
			});

			break;
		}

		default:
		{
			break;
		}

		}
	}

	delete[] bufferIn;
	delete[] bufferOut;

#endif

}

bool folder_erase(WCHAR *str) {
	WIN32_FIND_DATA fdFile;
	HANDLE hFind = NULL;

	wchar_t sPath[2048];

	//Specify a file mask. *.* = We want everything! 
	wsprintf(sPath, L"%s\\*", str);

	if ((hFind = FindFirstFile(sPath, &fdFile)) == INVALID_HANDLE_VALUE)
	{
		wprintf(L"Path not found: [%s]\n", str);
		return false;
	}

	do
	{

		if (wcscmp(fdFile.cFileName, L".") != 0
			&& wcscmp(fdFile.cFileName, L"..") != 0)
		{
			wsprintf(sPath, L"%s\\%s", str, fdFile.cFileName);

			//Is the entity a File or Folder? 
			if (fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				;
			}
			else {
				CFileObject *fo = new CFileObject(sPath);
				fo->forceErase(OverwritePattern::SINGLE_ONE_OVERWRITE);
				wprintf(L"File: %s\n", sPath);
			}
		}
	} while (FindNextFile(hFind, &fdFile)); //Find the next file. 

	FindClose(hFind); //Always, Always, clean things up! 

}