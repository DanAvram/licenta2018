#pragma once
#include <vector>
#include <random>
#include "utils.h"

using namespace std;

enum class OverwritePattern {
	SINGLE_ONE_OVERWRITE	= 1,
	US_AIRFORCE_5020		= 2,
	SCHNEIER				= 3,
	RCMP_TSSIT_OPS_II       = 4,
};



class RawPatternGenerator
{
private:
public:
	virtual void generate(unsigned char *buffer, size_t length) = 0;
};

class OneCharacterPatternGenerator : public RawPatternGenerator
{
	unsigned char value;
public:
	OneCharacterPatternGenerator(unsigned char aValue)
	{
		value = aValue;
	}

	virtual void generate(unsigned char *buffer, size_t length)
	{
		if (buffer != NULL)
			memset(buffer, value, length);
	}

};

class RandomCharacterPatternGenerator : public RawPatternGenerator
{
	std::mt19937 rng;
public:
	RandomCharacterPatternGenerator()
	{
		rng.seed(std::random_device()());
	}

	virtual void generate(unsigned char *buffer, size_t length)
	{
		std::uniform_int_distribution<std::mt19937::result_type> rand_value(0, 255);

		// TODO: Optimize
		for (int i = 0; i < length; i++)
		{
			buffer[i] = rand_value(rng);
		}
	}
};

class BaseGenerator
{
protected:
	vector<RawPatternGenerator*> generatorsList;
	int currentPattern = 0;

	/* Make it non-instantiable */
	BaseGenerator() {}
public:
	int getBuffer(unsigned char* buffer, size_t length) {
		if (buffer == NULL) {
			LOG_ERROR("Buffer is NULL");
			return -1;
		}
		if (currentPattern >= generatorsList.size()) {
			return -1;
		}
		generatorsList.at(currentPattern)->generate(buffer, length);

		return 0;

	}

	int nextPattern() {
		if (currentPattern >= generatorsList.size())
			return -1;
		currentPattern++;
		if (currentPattern >= generatorsList.size())
			return -1;
		return 0;
	}

	void reset()
	{
		currentPattern = 0;
	}

	~BaseGenerator()
	{
		while (generatorsList.size() > 0)
		{
			delete generatorsList.back();
			generatorsList.pop_back();
		}
	}
};

class SingleOneGenerator : public BaseGenerator {
public:
	SingleOneGenerator()
	{
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));
	}
};

class USAirForce5020Generator : public BaseGenerator {
public:
	USAirForce5020Generator()
	{
		generatorsList.push_back(new OneCharacterPatternGenerator(0x00));	// zeros
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));	// ones
		generatorsList.push_back(new OneCharacterPatternGenerator(0xCD));	// any character
	}
};

class SchneierGenerator : public BaseGenerator {
public:
	SchneierGenerator()
	{
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));	// zeros
		generatorsList.push_back(new OneCharacterPatternGenerator(0x00));	// ones
		generatorsList.push_back(new RandomCharacterPatternGenerator);	//  random
		generatorsList.push_back(new RandomCharacterPatternGenerator);	//  random
		generatorsList.push_back(new RandomCharacterPatternGenerator);	//  random
		generatorsList.push_back(new RandomCharacterPatternGenerator);	//  random
		generatorsList.push_back(new RandomCharacterPatternGenerator);	//  random
	}
};

class RCMP_TSSIT_OPS_IIGenerator : public BaseGenerator {
public:
	RCMP_TSSIT_OPS_IIGenerator()
	{
		generatorsList.push_back(new OneCharacterPatternGenerator(0x00));	// ones
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));	// zeros
		generatorsList.push_back(new OneCharacterPatternGenerator(0x00));	// ones
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));	// zeros
		generatorsList.push_back(new OneCharacterPatternGenerator(0x00));	// ones
		generatorsList.push_back(new OneCharacterPatternGenerator(0xFF));	// zeros
		generatorsList.push_back(new OneCharacterPatternGenerator(0xEA));	// ones

	}
};

static class GeneratorFactory
{
private:
	/* Non-instantiable*/
	GeneratorFactory();
public:
	static BaseGenerator* getPatternGenerator(OverwritePattern pattern)
	{
		switch (pattern)
		{
		case OverwritePattern::SINGLE_ONE_OVERWRITE:
			return new SingleOneGenerator();
			break;
		case OverwritePattern::US_AIRFORCE_5020:
			return new USAirForce5020Generator();
			break;
		case OverwritePattern::SCHNEIER:
			return new SchneierGenerator();
			break;
		case OverwritePattern::RCMP_TSSIT_OPS_II:
			return new RCMP_TSSIT_OPS_IIGenerator();
		default:
			break;
		}
	}
};