#pragma once

/* GNU General Public License version 3 */
/* https://www.codeproject.com/Articles/28314/Reading-and-Writing-to-Raw-Disk-Sectors */
/* Modified 06/06/2018*/


#include <stdio.h>
#include <tchar.h>
#include <WinSock2.h>
#include <windows.h>
#include <shlwapi.h>
#include <io.h>
#include <stdlib.h>
#include "utils.h"


#define METHOD_BUFFERED                 0
#define METHOD_IN_DIRECT                1
#define METHOD_OUT_DIRECT               2
#define METHOD_NEITHER                  3

#define FILE_ANY_ACCESS                 0
#define FILE_SPECIAL_ACCESS    (FILE_ANY_ACCESS)
#define FILE_READ_ACCESS          ( 0x0001 )    // file & pipe
#define FILE_WRITE_ACCESS         ( 0x0002 )    // file & pipe

typedef LONG	NTSTATUS;

#define STATUS_SUCCESS					((NTSTATUS)0x00000000L)
#define STATUS_INFO_LENGTH_MISMATCH		((NTSTATUS)0xC0000004L)

#pragma pack (push, 1)

typedef struct _DISK_LOCATION {
	BOOLEAN						bIsRawDiskObj;
	DWORD						dwDiskOrdinal;
	ULONGLONG					ullSectorNum;
} DISK_LOCATION, *PDISK_LOCATION;

typedef struct _STRING {
	USHORT  Length;
	USHORT  MaximumLength;
	PCHAR  Buffer;
} ANSI_STRING, *PANSI_STRING;

typedef struct _UNICODE_STRING {
	USHORT  Length;
	USHORT  MaximumLength;
	PWSTR  Buffer;
} UNICODE_STRING, *PUNICODE_STRING;

#pragma pack (pop)

#define CTL_CODE( DeviceType, Function, Method, Access ) (                 \
    ((DeviceType) << 16) | ((Access) << 14) | ((Function) << 2) | (Method) \
)

#define	DRV_NAME		"sectorio"
#define DRV_FILENAME	"sectorio.sys"

#define SECTOR_IO_DEVICE       0x8000

#define IOCTL_SECTOR_READ		CTL_CODE(SECTOR_IO_DEVICE, 0x800, METHOD_BUFFERED, FILE_READ_ACCESS | FILE_WRITE_ACCESS)
#define IOCTL_SECTOR_WRITE		CTL_CODE(SECTOR_IO_DEVICE, 0x801, METHOD_BUFFERED, FILE_READ_ACCESS | FILE_WRITE_ACCESS)
#define IOCTL_GET_SECTOR_SIZE	CTL_CODE(SECTOR_IO_DEVICE, 0x802, METHOD_BUFFERED, FILE_READ_ACCESS | FILE_WRITE_ACCESS)



/*MODIFY !!!
 THIS IS NOT POO :))*/

/* WARNING: This class works directly with the windows disk objects.
			Bad things can happen if not used properly					*/
static class CDriverAccess
{
private:
	//HANDLE hDevice;
	
public:
	static BOOL read_sector(unsigned char *buffer, unsigned int device_number, LARGE_INTEGER sector_no);
	static BOOL write_sector(unsigned char *buffer, unsigned int device_number, LARGE_INTEGER sector_no);
private:
	static BOOL getLoadDriverPriv();
	static BOOL setupRegistry();
	static BOOL loadDriver();
	static void cleanupDriver(void);
	static BOOL unloadDriver();
	static void uninstallDriver(void);
	static HANDLE openDriver(void);
};

