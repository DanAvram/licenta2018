/*
 * NTFS Attribute Classes
 * 
 * Copyright(C) 2010 cyb70289 <cyb70289@gmail.com>
 */

#ifndef	__NTFS_ATTRIBUTE_H_CYB70289
#define	__NTFS_ATTRIBUTE_H_CYB70289

////////////////////////////////
// List to hold parsed DataRuns
////////////////////////////////
typedef struct tagDataRun_Entry
{
	LONGLONG			LCN;		// -1 to indicate sparse data
	ULONGLONG			Clusters;
	ULONGLONG			StartVCN;
	ULONGLONG			LastVCN;
} DataRun_Entry;
typedef class CSList<DataRun_Entry> CDataRunList;

////////////////////////////////////
// List to hold Index Entry objects
////////////////////////////////////
class CIndexEntry;
typedef class CSList<CIndexEntry> CIndexEntryList;

////////////////////////////////
// Attributes base class
////////////////////////////////
class CAttrBase
{
public:
	CAttrBase(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttrBase();

protected:
	const ATTR_HEADER_COMMON *AttrHeader;
	WORD _SectorSize;
	DWORD _ClusterSize;
	DWORD _IndexBlockSize;
	HANDLE _hVolume;
	const CFileRecord *FileRecord;

public:
	__inline const ATTR_HEADER_COMMON* GetAttrHeader() const;
	__inline DWORD GetAttrType() const;
	__inline DWORD GetAttrTotalSize() const;
	__inline BOOL IsNonResident() const;
	__inline WORD GetAttrFlags() const;
	int GetAttrName(char *buf, DWORD bufLen) const;
	int GetAttrName(wchar_t *buf, DWORD bufLen) const;
	__inline BOOL IsUnNamed() const;

protected:
	virtual __inline BOOL IsDataRunOK() const = 0;

public:
	virtual __inline ULONGLONG GetDataSize(ULONGLONG *allocSize = NULL) const = 0;
	virtual BOOL ReadData(const ULONGLONG &offset, void *bufv, DWORD bufLen, DWORD *actural) const = 0;
};	// CAttrBase


__inline const ATTR_HEADER_COMMON* CAttrBase::GetAttrHeader() const
{
	return AttrHeader;
}

__inline DWORD CAttrBase::GetAttrType() const
{
	return AttrHeader->Type;
}

__inline DWORD CAttrBase::GetAttrTotalSize() const
{
	return AttrHeader->TotalSize;
}

__inline BOOL CAttrBase::IsNonResident() const
{
	return AttrHeader->NonResident;
}

__inline WORD CAttrBase::GetAttrFlags() const
{
	return AttrHeader->Flags;
}

// Verify if this attribute is unnamed
// Useful in analyzing MultiStream files
__inline BOOL CAttrBase::IsUnNamed() const
{
	return (AttrHeader->NameLength == 0);
}

////////////////////////////////
// Resident Attributes
////////////////////////////////
class CAttrResident : public CAttrBase
{
public:
	CAttrResident(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttrResident();

protected:
	const ATTR_HEADER_RESIDENT *AttrHeaderR;
	const void *AttrBody;	// Points to Resident Data
	DWORD AttrBodySize;		// Attribute Data Size

	virtual __inline BOOL IsDataRunOK() const;

public:
	virtual __inline ULONGLONG GetDataSize(ULONGLONG *allocSize = NULL) const;
	virtual BOOL ReadData(const ULONGLONG &offset, void *bufv, DWORD bufLen, DWORD *actural) const;
};	// CAttrResident

__inline BOOL CAttrResident::IsDataRunOK() const
{
	return TRUE;	// Always OK for a resident attribute
}

// Return Actural Data Size
// *allocSize = Allocated Size
__inline ULONGLONG CAttrResident::GetDataSize(ULONGLONG *allocSize) const
{
	if (allocSize)
		*allocSize = AttrBodySize;

	return (ULONGLONG)AttrBodySize;
}


////////////////////////////////
// NonResident Attributes
////////////////////////////////
class CAttrNonResident : public CAttrBase
{
public:
	CAttrNonResident(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttrNonResident();

protected:
	const ATTR_HEADER_NON_RESIDENT *AttrHeaderNR;
	CDataRunList DataRunList;

private:
	BOOL bDataRunOK;
	BYTE *UnalignedBuf;	// Buffer to hold not cluster aligned data
	BOOL PickData(const BYTE **dataRun, LONGLONG *length, LONGLONG *LCNOffset);
	BOOL ParseDataRun();
	BOOL ReadClusters(void *buf, DWORD clusters, LONGLONG lcn);
	BOOL ReadVirtualClusters(ULONGLONG vcn, DWORD clusters,
		void *bufv, DWORD bufLen, DWORD *actural);

protected:
	virtual __inline BOOL IsDataRunOK() const;

public:
	virtual __inline ULONGLONG GetDataSize(ULONGLONG *allocSize = NULL) const;
	virtual BOOL ReadData(const ULONGLONG &offset, void *bufv, DWORD bufLen, DWORD *actural) const;
	__inline const CDataRunList &getDataRunList() const;
};	// CAttrNonResident

	// Judge if the DataRun is successfully parsed
__inline BOOL CAttrNonResident::IsDataRunOK() const
{
	return bDataRunOK;
}

// Return Actural Data Size
// *allocSize = Allocated Size
__inline ULONGLONG CAttrNonResident::GetDataSize(ULONGLONG *allocSize) const
{
	if (allocSize)
		*allocSize = AttrHeaderNR->AllocSize;

	return AttrHeaderNR->RealSize;
}

__inline const CDataRunList &CAttrNonResident::getDataRunList() const
{
	/* TODO: Maybe create a new list and return that?*/
	return DataRunList;
}


///////////////////////////////////
// Attribute: Standard Information
///////////////////////////////////
class CAttr_StdInfo : public CAttrResident
{
public:
	CAttr_StdInfo(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttr_StdInfo();

private:
	const ATTR_STANDARD_INFORMATION *StdInfo;

public:
	void GetFileTime(FILETIME *writeTm, FILETIME *createTm = NULL, FILETIME *accessTm = NULL) const;
	__inline DWORD GetFilePermission() const;
	__inline BOOL IsReadOnly() const;
	__inline BOOL IsHidden() const;
	__inline BOOL IsSystem() const;
	__inline BOOL IsCompressed() const;
	__inline BOOL IsEncrypted() const;
	__inline BOOL IsSparse() const;

	static void UTC2Local(const ULONGLONG &ultm, FILETIME *lftm);
};	// CAttr_StdInfo

__inline DWORD CAttr_StdInfo::GetFilePermission() const
{
	return StdInfo->Permission;
}

__inline BOOL CAttr_StdInfo::IsReadOnly() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_READONLY);
}

__inline BOOL CAttr_StdInfo::IsHidden() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_HIDDEN);
}

__inline BOOL CAttr_StdInfo::IsSystem() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_SYSTEM);
}

__inline BOOL CAttr_StdInfo::IsCompressed() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_COMPRESSED);
}

__inline BOOL CAttr_StdInfo::IsEncrypted() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_ENCRYPTED);
}

__inline BOOL CAttr_StdInfo::IsSparse() const
{
	return ((StdInfo->Permission) & ATTR_STDINFO_PERMISSION_SPARSE);
}

////////////////////////////////////////
// FileName helper class
// used by FileName and IndexEntry
////////////////////////////////////////
class CFileName
{
public:
	CFileName(ATTR_FILE_NAME *fn = NULL);
	virtual ~CFileName();

protected:
	const ATTR_FILE_NAME *FileName;	// May be NULL for an IndexEntry
	wchar_t *FileNameWUC;	// Uppercase Unicode File Name, used to compare file names
	int FileNameLength;
	BOOL IsCopy;

	__inline void SetFileName(ATTR_FILE_NAME *fn);
	void CFileName::CopyFileName(const CFileName *fn, const ATTR_FILE_NAME *afn);

private:
	void GetFileNameWUC();

public:
	int Compare(const wchar_t *fn) const;
	int Compare(const char *fn) const;

	__inline ULONGLONG GetFileSize() const;
	__inline DWORD GetFilePermission() const;
	__inline BOOL IsReadOnly() const;
	__inline BOOL IsHidden() const;
	__inline BOOL IsSystem() const;
	__inline BOOL IsDirectory() const;
	__inline BOOL IsCompressed() const;
	__inline BOOL IsEncrypted() const;
	__inline BOOL IsSparse() const;

	int GetFileName(char *buf, DWORD bufLen) const;
	int GetFileName(wchar_t *buf, DWORD bufLen) const;
	__inline BOOL HasName() const;
	BOOL IsWin32Name() const;

	void GetFileTime(FILETIME *writeTm, FILETIME *createTm = NULL, FILETIME *accessTm = NULL) const;
};	// CFileName
__inline ULONGLONG CFileName::GetFileSize() const
{
	return FileName ? FileName->RealSize : 0;
}

__inline DWORD CFileName::GetFilePermission() const
{
	return FileName ? FileName->Flags : 0;
}

__inline BOOL CFileName::IsReadOnly() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_READONLY) : FALSE;
}

__inline BOOL CFileName::IsHidden() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_HIDDEN) : FALSE;
}

__inline BOOL CFileName::IsSystem() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_SYSTEM) : FALSE;
}

__inline BOOL CFileName::IsDirectory() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_DIRECTORY) : FALSE;
}

__inline BOOL CFileName::IsCompressed() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_COMPRESSED) : FALSE;
}

__inline BOOL CFileName::IsEncrypted() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_ENCRYPTED) : FALSE;
}

__inline BOOL CFileName::IsSparse() const
{
	return FileName ? ((FileName->Flags) & ATTR_FILENAME_FLAG_SPARSE) : FALSE;
}



__inline BOOL CFileName::HasName() const
{
	return FileNameLength > 0;
}



////////////////////////////////
// Attribute: File Name
////////////////////////////////
class CAttr_FileName : public CAttrResident, public CFileName
{
public:
	CAttr_FileName(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : CAttrResident(ahc, fr)
	{
		NTFS_TRACE("Attribute: File Name\n");

		SetFileName((ATTR_FILE_NAME*)AttrBody);
	}

	virtual ~CAttr_FileName()
	{
		NTFS_TRACE("CAttr_FileName deleted\n");
	}

private:
	// File permission and time in $FILE_NAME only updates when the filename changes
	// So hide these functions to prevent user from getting the error information
	// Standard Information and IndexEntry keeps the most recent file time and permission infomation
	void GetFileTime(FILETIME *writeTm, FILETIME *createTm = NULL, FILETIME *accessTm = NULL) const {}
	__inline DWORD GetFilePermission(){}
	__inline BOOL IsReadOnly() const {}
	__inline BOOL IsHidden() const {}
	__inline BOOL IsSystem() const {}
	__inline BOOL IsCompressed() const {}
	__inline BOOL IsEncrypted() const {}
	__inline BOOL IsSparse() const {}
};	// CAttr_FileName


//////////////////////////////////
// Attribute: Volume Information
//////////////////////////////////
class CAttr_VolInfo : public CAttrResident
{
public:
	CAttr_VolInfo(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : CAttrResident(ahc, fr)
	{
		NTFS_TRACE("Attribute: Volume Information\n");

		VolInfo = (ATTR_VOLUME_INFORMATION*)AttrBody;
	}

	virtual ~CAttr_VolInfo()
	{
		NTFS_TRACE("CAttr_VolInfo deleted\n");
	}

private:
	const ATTR_VOLUME_INFORMATION *VolInfo;

public:
	// Get NTFS Volume Version
	__inline WORD GetVersion()
	{
		return MAKEWORD(VolInfo->MinorVersion, VolInfo->MajorVersion);
	}
}; // CAttr_VolInfo


///////////////////////////
// Attribute: Volume Name
///////////////////////////
class CAttr_VolName : public CAttrResident
{
public:
	CAttr_VolName(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : CAttrResident(ahc, fr)
	{
		NTFS_TRACE("Attribute: Volume Name\n");

		NameLength = AttrBodySize >> 1;
		VolNameU = new wchar_t[NameLength+1];
		VolNameA = new char[NameLength+1];

		memcpy(VolNameU, AttrBody, AttrBodySize);
		VolNameU[NameLength] = wchar_t('\0');

		int len = WideCharToMultiByte(CP_ACP, 0, VolNameU, NameLength,
			VolNameA, NameLength, NULL, NULL);
		VolNameA[NameLength] = '\0';
	}

	virtual ~CAttr_VolName()
	{
		NTFS_TRACE("CAttr_VolName deleted\n");

		delete VolNameU;
		delete VolNameA;
	}

private:
	wchar_t *VolNameU;
	char *VolNameA;
	DWORD NameLength;

public:
	// Get NTFS Volume Unicode Name
	__inline int GetName(wchar_t *buf, DWORD len) const
	{
		if (len < NameLength)
			return -1*NameLength;	// buffer too small

		wcsncpy(buf, VolNameU, NameLength+1);
		return NameLength;
	}

	// ANSI Name
	__inline int GetName(char *buf, DWORD len) const
	{
		if (len < NameLength)
			return -1*NameLength;	// buffer too small

		strncpy(buf, VolNameA, NameLength+1);
		return NameLength;
	}
}; // CAttr_VolInfo


/////////////////////////////////////
// Attribute: Data
/////////////////////////////////////
template <class TYPE_RESIDENT>
class CAttr_Data : public TYPE_RESIDENT
{
public:
	CAttr_Data(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : TYPE_RESIDENT(ahc, fr)
	{
		NTFS_TRACE1("Attribute: Data (%sResident)\n", IsNonResident() ? "Non" : "");
	}

	virtual ~CAttr_Data()
	{
		NTFS_TRACE("CAttr_Data deleted\n");
	}
};	// CAttr_Data


/////////////////////////////
// Index Entry helper class
/////////////////////////////
class CIndexEntry : public CFileName
{
public:
	CIndexEntry()
	{
		NTFS_TRACE("Index Entry\n");

		IsDefault = TRUE;

		IndexEntry = NULL;
		SetFileName(NULL);
	}

	CIndexEntry(const INDEX_ENTRY *ie)
	{
		NTFS_TRACE("Index Entry\n");

		IsDefault = FALSE;

		_ASSERT(ie);
		IndexEntry = ie;

		if (IsSubNodePtr())
		{
			NTFS_TRACE("Points to sub-node\n");
		}

		if (ie->StreamSize)
		{
			SetFileName((ATTR_FILE_NAME*)(ie->Stream));
		}
		else
		{
			NTFS_TRACE("No FileName stream found\n");
		}
	}

	virtual ~CIndexEntry()
	{
		// Never touch *IndexEntry here if IsCopy == FALSE !
		// As the memory have been deallocated by ~CIndexBlock()

		if (IsCopy && IndexEntry)
			delete (void*)IndexEntry;

		NTFS_TRACE("CIndexEntry deleted\n");
	}

private:
	BOOL IsDefault;

protected:
	const INDEX_ENTRY *IndexEntry;

public:
	// Use with caution !
	CIndexEntry& operator = (const CIndexEntry &ieClass)
	{
		if (!IsDefault)
		{
			NTFS_TRACE("Cannot call this routine\n");
			return *this;
		}

		NTFS_TRACE("Index Entry Copied\n");

		IsCopy = TRUE;

		if (IndexEntry)
		{
			delete (void*)IndexEntry;
			IndexEntry = NULL;
		}

		const INDEX_ENTRY *ie = ieClass.IndexEntry;
		_ASSERT(ie && (ie->Size > 0));

		IndexEntry = (INDEX_ENTRY*)new BYTE[ie->Size];
		memcpy((void*)IndexEntry, ie, ie->Size);
		CopyFileName(&ieClass, (ATTR_FILE_NAME*)(IndexEntry->Stream));

		return *this;
	}

	__inline ULONGLONG GetFileReference() const
	{
		if (IndexEntry)
			return IndexEntry->FileReference & 0x0000FFFFFFFFFFFFUL;
		else
			return (ULONGLONG)-1;
	}

	__inline BOOL IsSubNodePtr() const
	{
		if (IndexEntry)
			return (IndexEntry->Flags & INDEX_ENTRY_FLAG_SUBNODE);
		else
			return FALSE;
	}

	__inline ULONGLONG GetSubNodeVCN() const
	{
		if (IndexEntry)
			return *(ULONGLONG*)((BYTE*)IndexEntry + IndexEntry->Size - 8);
		else
			return (ULONGLONG)-1;
	}
};	// CIndexEntry


///////////////////////////////
// Index Block helper class
///////////////////////////////
class CIndexBlock : public CIndexEntryList
{
public:
	CIndexBlock()
	{
		NTFS_TRACE("Index Block\n");

		IndexBlock = NULL;
	}

	virtual ~CIndexBlock()
	{
		NTFS_TRACE("IndexBlock deleted\n");

		if (IndexBlock)
			delete IndexBlock;
	}

private:
	INDEX_BLOCK *IndexBlock;

public:
	INDEX_BLOCK *AllocIndexBlock(DWORD size)
	{
		// Free previous data if any
		if (GetCount() > 0)
			RemoveAll();
		if (IndexBlock)
			delete IndexBlock;

		IndexBlock = (INDEX_BLOCK*)new BYTE[size];

		return IndexBlock;
	}
};	// CIndexBlock


/////////////////////////////////////
// Attribute: Index Root (Resident)
/////////////////////////////////////
class CAttr_IndexRoot : public CAttrResident, public CIndexEntryList
{
public:
	CAttr_IndexRoot(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttr_IndexRoot();

private:
	const ATTR_INDEX_ROOT *IndexRoot;

	void ParseIndexEntries();

public:
	__inline BOOL IsFileName() const;
};	// CAttr_IndexRoot

	// Check if this IndexRoot contains FileName or IndexView
__inline BOOL CAttr_IndexRoot::IsFileName() const
{
	return (IndexRoot->AttrType == ATTR_TYPE_FILE_NAME);
}


/////////////////////////////////////////////
// Attribute: Index Allocation (NonResident)
/////////////////////////////////////////////
class CAttr_IndexAlloc : public CAttrNonResident
{
public:
	CAttr_IndexAlloc(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttr_IndexAlloc();

private:
	ULONGLONG IndexBlockCount;

	BOOL PatchUS(WORD *sector, int sectors, WORD usn, WORD *usarray);

public:
	__inline ULONGLONG GetIndexBlockCount();
	BOOL ParseIndexBlock(const ULONGLONG &vcn, CIndexBlock &ibClass);
};	// CAttr_IndexAlloc



__inline ULONGLONG CAttr_IndexAlloc::GetIndexBlockCount()
{
	return IndexBlockCount;
}

////////////////////////////////////////////
// Attribute: Bitmap
////////////////////////////////////////////
template <class TYPE_RESIDENT>
class CAttr_Bitmap : public TYPE_RESIDENT
{
public:
	CAttr_Bitmap(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttr_Bitmap();

private:
	ULONGLONG BitmapSize;	// Bitmap data size
	BYTE *BitmapBuf;		// Bitmap data buffer
	LONGLONG CurrentCluster;

public:
	BOOL IsClusterFree(const ULONGLONG &cluster) const;
};	// CAttr_Bitmap

template <typename TYPE_RESIDENT>
CAttr_Bitmap<TYPE_RESIDENT>::CAttr_Bitmap(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : TYPE_RESIDENT(ahc, fr)
{
	NTFS_TRACE1("Attribute: Bitmap (%sResident)\n", IsNonResident() ? "Non" : "");

	CurrentCluster = -1;

	if (IsDataRunOK())
	{
		BitmapSize = GetDataSize();

		if (IsNonResident())
			BitmapBuf = new BYTE[_ClusterSize];
		else
		{
			BitmapBuf = new BYTE[(DWORD)BitmapSize];

			DWORD len;
			if (!(ReadData(0, BitmapBuf, (DWORD)BitmapSize, &len)
				&& len == (DWORD)BitmapSize))
			{
				BitmapBuf = NULL;
				NTFS_TRACE("Read Resident Bitmap data failed\n");
			}
			else
			{
				NTFS_TRACE1("%u bytes of resident Bitmap data read\n", len);
			}
		}
	}
	else
	{
		BitmapSize = 0;
		BitmapBuf = 0;
	}
}

template <class TYPE_RESIDENT>
CAttr_Bitmap<TYPE_RESIDENT>::~CAttr_Bitmap()
{
	if (BitmapBuf)
		delete BitmapBuf;

	NTFS_TRACE("CAttr_Bitmap deleted\n");
}

// Verify if a single cluster is free
template <class TYPE_RESIDENT>
BOOL CAttr_Bitmap<TYPE_RESIDENT>::IsClusterFree(const ULONGLONG &cluster) const
{
	if (!IsDataRunOK() || !BitmapBuf)
		return FALSE;

	if (IsNonResident())
	{
		LONGLONG idx = (LONGLONG)cluster >> 3;
		DWORD clusterSize = ((CNTFSVolume*)Volume)->GetClusterSize();

		LONGLONG clusterOffset = idx/clusterSize;
		cluster -= (clusterOffset*clusterSize*8);

		// Read one cluster of data if buffer mismatch
		if (CurrentCluster != clusterOffset)
		{
			DWORD len;
			if (ReadData(clusterOffset, BitmapBuf, clusterSize, &len) && len == clusterSize)
			{
				CurrentCluster = clusterOffset;
			}
			else
			{
				CurrentCluster = -1;
				return FALSE;
			}
		}
	}

	// All the Bitmap data is already in BitmapBuf
	DWORD idx = (DWORD)(cluster >> 3);
	if (IsNonResident() == FALSE)
	{
		if (idx >= BitmapSize)
			return TRUE;	// Resident data bounds check error
	}

	BYTE fac = (BYTE)(cluster % 8);

	return ((BitmapBuf[idx] & (1<<fac)) == 0);
}


////////////////////////////////////////////
// List to hold external File Records
////////////////////////////////////////////
typedef CSList<CFileRecord> CFileRecordList;

////////////////////////////////////////////
// Attribute: Attribute List
////////////////////////////////////////////
template <class TYPE_RESIDENT>
class CAttr_AttrList : public TYPE_RESIDENT
{
public:
	CAttr_AttrList(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr);
	virtual ~CAttr_AttrList();

private:
	CFileRecordList FileRecordList;
};	// CAttr_AttrList

template <class TYPE_RESIDENT>
CAttr_AttrList<TYPE_RESIDENT>::CAttr_AttrList(const ATTR_HEADER_COMMON *ahc, const CFileRecord *fr) : TYPE_RESIDENT(ahc, fr)
{
	NTFS_TRACE("Attribute: Attribute List\n");
	if (fr->FileReference == (ULONGLONG)-1)
		return;

	ULONGLONG offset = 0;
	DWORD len;
	ATTR_ATTRIBUTE_LIST alRecord;

	while (ReadData(offset, &alRecord, sizeof(ATTR_ATTRIBUTE_LIST), &len) &&
		len == sizeof(ATTR_ATTRIBUTE_LIST))
	{
		if (ATTR_INDEX(alRecord.AttrType) > ATTR_NUMS)
		{
			NTFS_TRACE("Attribute List parse error1\n");
			break;
		}

		NTFS_TRACE1("Attribute List: 0x%04x\n", alRecord.AttrType);

		ULONGLONG recordRef = alRecord.BaseRef & 0x0000FFFFFFFFFFFFUL;
		if (recordRef != fr->FileReference)	// Skip contained attributes
		{
			DWORD am = ATTR_MASK(alRecord.AttrType);
			if (am & fr->AttrMask)	// Skip unwanted attributes
			{
				CFileRecord *frnew = new CFileRecord(fr->Volume);
				FileRecordList.InsertEntry(frnew);

				frnew->AttrMask = am;
				if (!frnew->ParseFileRecord(recordRef))
				{
					NTFS_TRACE("Attribute List parse error2\n");
					break;
				}
				frnew->ParseAttrs();

				// Insert new found AttrList to fr->AttrList
				const CAttrBase *ab = (CAttrBase*)frnew->FindFirstAttr(alRecord.AttrType);
				while (ab)
				{
					CAttrList *al = (CAttrList*)&fr->AttrList[ATTR_INDEX(alRecord.AttrType)];
					al->InsertEntry((CAttrBase*)ab);
					ab = frnew->FindNextAttr(alRecord.AttrType);
				}

				// Throw away frnew->AttrList entries to prevent free twice (fr will delete them)
				frnew->AttrList[ATTR_INDEX(alRecord.AttrType)].ThrowAll();
			}
		}

		offset += alRecord.RecordSize;
	}
}

template <class TYPE_RESIDENT>
CAttr_AttrList<TYPE_RESIDENT>::~CAttr_AttrList()
{
	NTFS_TRACE("CAttr_AttrList deleted\n");
}

#endif
