/*
 * NTFS Volume and File Record Class
 * 
 * Copyright(C) 2010 cyb70289 <cyb70289@gmail.com>
 */

#ifndef	__NTFS_FILERECORD_H_CYB70289
#define	__NTFS_FILERECORD_H_CYB70289


///////////////////////////////////////
// NTFS Volume forward declaration
///////////////////////////////////////
class CNTFSVolume
{
public:
	CNTFSVolume(_TCHAR volume);
	virtual ~CNTFSVolume();

	friend class CFileRecord;
	friend class CAttrBase;

private:
	WORD SectorSize;
	DWORD ClusterSize;
	DWORD FileRecordSize;
	DWORD IndexBlockSize;
	ULONGLONG MFTAddr;
	HANDLE hVolume;
	BOOL VolumeOK;
	ATTR_RAW_CALLBACK AttrRawCallBack[ATTR_NUMS];
	/* Not supported for multiple extents */
	DWORD volumeExtentsBufferSize;
	PVOLUME_DISK_EXTENTS volumeExtentBuffer;
	WORD Version;

	// MFT file records ($MFT file itself) may be fragmented
	// Get $MFT Data attribute to translate FileRecord to correct disk offset
	CFileRecord *MFTRecord;		// $MFT File Record
	const CAttrBase *MFTData;	// $MFT Data Attribute

	BOOL OpenVolume(_TCHAR volume);

public:
	__inline BOOL IsVolumeOK() const;
	__inline WORD GetVersion() const;
	ULONGLONG GetRecordsCount() const;
	ULONGLONG GetVolumeDiskOffset() const;

	__inline DWORD GetSectorSize() const;
	__inline DWORD GetClusterSize() const;
	__inline DWORD GetFileRecordSize() const;
	__inline DWORD GetIndexBlockSize() const;
	__inline ULONGLONG GetMFTAddr() const;

	BOOL InstallAttrRawCB(DWORD attrType, ATTR_RAW_CALLBACK cb);
	__inline void ClearAttrRawCB();
};	// CNTFSVolume


////////////////////////////////////////////
// List to hold Attributes of the same type
////////////////////////////////////////////
typedef class CSList<CAttrBase> CAttrList;

// It seems VC6.0 doesn't support template class friends
#if	_MSC_VER <= 1200
class CAttrResident;
class CAttrNonResident;
template <class TYPE_RESIDENT> class CAttr_AttrList;
#endif


////////////////////////////////
// Process a single File Record
////////////////////////////////
class CFileRecord
{
public:
	CFileRecord(const CNTFSVolume *volume);
	virtual ~CFileRecord();

	friend class CAttrBase;
#if	_MSC_VER <= 1200
	// Walk around VC6.0 compiler defect
	friend class CAttr_AttrList<CAttrResident>;
	friend class CAttr_AttrList<CAttrNonResident>;
#else
	template <class TYPE_RESIDENT> friend class CAttr_AttrList;		// Won't compiler in VC6.0, why?
#endif

private:
	const CNTFSVolume *Volume;
	FILE_RECORD_HEADER *FileRecord;
	ULONGLONG FileReference;
	ATTR_RAW_CALLBACK AttrRawCallBack[ATTR_NUMS];
	DWORD AttrMask;
	CAttrList AttrList[ATTR_NUMS];	// Attributes

	void ClearAttrs();
	BOOL PatchUS(WORD *sector, int sectors, WORD usn, WORD *usarray);
	__inline void UserCallBack(DWORD attType, ATTR_HEADER_COMMON *ahc, BOOL *bDiscard);
	CAttrBase* AllocAttr(ATTR_HEADER_COMMON *ahc, BOOL *bUnhandled);
	BOOL ParseAttr(ATTR_HEADER_COMMON *ahc);
	FILE_RECORD_HEADER* ReadFileRecord(ULONGLONG &fileRef);
	BOOL VisitIndexBlock(const ULONGLONG &vcn, const _TCHAR *fileName, CIndexEntry &ieFound) const;
	void TraverseSubNode(const ULONGLONG &vcn, SUBENTRY_CALLBACK seCallBack) const;

public:
	BOOL ParseFileRecord(ULONGLONG fileRef);
	BOOL ParseAttrs();

	BOOL InstallAttrRawCB(DWORD attrType, ATTR_RAW_CALLBACK cb);
	__inline void ClearAttrRawCB();

	__inline void SetAttrMask(DWORD mask);
	void TraverseAttrs(ATTRS_CALLBACK attrCallBack, void *context);
	__inline const CAttrBase* FindFirstAttr(DWORD attrType) const;
	const CAttrBase* FindNextAttr(DWORD attrType) const;

	int GetFileName(_TCHAR *buf, DWORD bufLen) const;
	ULONGLONG GetFileSize() const;
	void GetFileTime(FILETIME *writeTm, FILETIME *createTm = NULL, FILETIME *accessTm = NULL) const;

	void TraverseSubEntries(SUBENTRY_CALLBACK seCallBack) const;
	const BOOL FindSubEntry(const _TCHAR *fileName, CIndexEntry &ieFound) const;
	const CAttrBase* FindStream(_TCHAR *name = NULL);

	__inline BOOL IsDeleted() const;
	__inline BOOL IsDirectory() const;
	__inline ULONGLONG GetFileRefference() const { return FileReference; }
	BOOL IsReadOnly() const;
	BOOL IsHidden() const;
	BOOL IsSystem() const;
	BOOL IsCompressed() const;
	BOOL IsEncrypted() const;
	BOOL IsSparse() const;
};	// CFileRecord

	// Check if it's deleted or in use
__inline BOOL CFileRecord::IsDeleted() const
{
	return !(FileRecord->Flags & FILE_RECORD_FLAG_INUSE);
}

// Check if it's a directory
__inline BOOL CFileRecord::IsDirectory() const
{
	return FileRecord->Flags & FILE_RECORD_FLAG_DIR;
}

// Clear all Attribute CallBack routines
__inline void CFileRecord::ClearAttrRawCB()
{
	for (int i = 0; i < ATTR_NUMS; i++)
		AttrRawCallBack[i] = NULL;
}

// Choose attributes to handle, unwanted attributes will be discarded silently
__inline void CFileRecord::SetAttrMask(DWORD mask)
{
	// Standard Information and Attribute List is needed always
	AttrMask = mask | MASK_STANDARD_INFORMATION | MASK_ATTRIBUTE_LIST;
}

// Find Attributes
__inline const CAttrBase* CFileRecord::FindFirstAttr(DWORD attrType) const
{
	DWORD attrIdx = ATTR_INDEX(attrType);

	return attrIdx < ATTR_NUMS ? AttrList[attrIdx].FindFirstEntry() : NULL;
}

// Call user defined Callback routines for an attribute
__inline void CFileRecord::UserCallBack(DWORD attType, ATTR_HEADER_COMMON *ahc, BOOL *bDiscard)
{
	*bDiscard = FALSE;

	if (AttrRawCallBack[attType])
		AttrRawCallBack[attType](ahc, bDiscard);
	else if (Volume->AttrRawCallBack[attType])
		Volume->AttrRawCallBack[attType](ahc, bDiscard);
}

// Check if Volume is successfully opened
__inline BOOL CNTFSVolume::IsVolumeOK() const
{
	return VolumeOK;
}



// Get NTFS volume version
__inline WORD CNTFSVolume::GetVersion() const
{
	return Version;
}



// Get BPB information

__inline DWORD CNTFSVolume::GetSectorSize() const
{
	return SectorSize;
}

__inline DWORD CNTFSVolume::GetClusterSize() const
{
	return ClusterSize;
}

__inline DWORD CNTFSVolume::GetFileRecordSize() const
{
	return FileRecordSize;
}

__inline DWORD CNTFSVolume::GetIndexBlockSize() const
{
	return IndexBlockSize;
}

// Get MFT starting address
__inline ULONGLONG CNTFSVolume::GetMFTAddr() const
{
	return MFTAddr;
}


// Clear all Attribute CallBack routines
__inline void CNTFSVolume::ClearAttrRawCB()
{
	for (int i = 0; i < ATTR_NUMS; i++)
		AttrRawCallBack[i] = NULL;
}


#endif
