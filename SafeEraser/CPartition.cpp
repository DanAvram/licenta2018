#include "CPartition.h"
#include <stdio.h>
#include <Shlwapi.h>  //wsnprintf()

#include "CDriverAccess.h"





CPartition::CPartition(CPhysicalDevice* aDisk, PARTITION_INFORMATION_EX aPartitionInformation)
{
	disk = aDisk;
	partitionInformation = aPartitionInformation;
}


CPartition::~CPartition()
{
}

void CPartition::unmountPartitionVolumes()
{
	TCHAR volumes[64];
	if (checkMountedVolumes(volumes))
	{
		HANDLE CHandle = CreateFile(
			volumes,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);

		if (CHandle == INVALID_HANDLE_VALUE)
		{
			LOG_ERROR(L"%s", ShowError());
			LOG_ERROR(L"%s", L"Volume couldn't be open");
			return;
		}

		if (!DeviceIoControl(
			(HANDLE)CHandle,             // handle to a volume
			(DWORD)FSCTL_LOCK_VOLUME,    // dwIoControlCode
			NULL,                        // lpInBuffer
			0,                           // nInBufferSize
			NULL,                        // lpOutBuffer
			0,                           // nOutBufferSize
			NULL,						 // number of bytes returned
			NULL						 // OVERLAPPED structure
		))
		{
			LOG_ERROR(L"%s", ShowError());
		}

		if (!DeviceIoControl(
			(HANDLE)CHandle,             // handle to a volume
			(DWORD)FSCTL_DISMOUNT_VOLUME,    // dwIoControlCode
			NULL,                        // lpInBuffer
			0,                           // nInBufferSize
			NULL,                        // lpOutBuffer
			0,                           // nOutBufferSize
			NULL,						 // number of bytes returned
			NULL						 // OVERLAPPED structure
		))
		{
			LOG_ERROR(L"%s", ShowError());
		}
	}
}

BOOL CPartition::checkMountedVolumes(LPTSTR mountedVoumes)
{
	DWORD logicalDriveStringsLength;
	TCHAR logicalDriveBuffer[512];

	DWORD volumeExtentsBufferSize = sizeof(VOLUME_DISK_EXTENTS) + (4 * sizeof(DISK_EXTENT));
	PVOLUME_DISK_EXTENTS volumeExtentBuffer = (VOLUME_DISK_EXTENTS*)malloc(volumeExtentsBufferSize);

	logicalDriveStringsLength = GetLogicalDriveStrings(
		512,
		logicalDriveBuffer
	);
	for (int i = 0; i < logicalDriveStringsLength; i += 4)
	{
		TCHAR currentDriveLetter;
		TCHAR drivePath[8];

		currentDriveLetter = logicalDriveBuffer[i];
		wsprintf(drivePath, L"\\\\.\\%c:", currentDriveLetter);

		HANDLE CHandle = CreateFile(
			drivePath,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);

		if (!DeviceIoControl((HANDLE)CHandle,     // handle to device
			IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS, // dwIoControlCode
			NULL,                                 // lpInBuffer
			0,                                    // nInBufferSize
			volumeExtentBuffer,                   // output buffer
			volumeExtentsBufferSize,              // size of output buffer
			NULL,							   	  // number of bytes returned
			NULL								  // OVERLAPPED structure
		))
		{
			CloseHandle(CHandle);
			LOG_ERROR("%s", ShowError());
			return -1;
		}
		CloseHandle(CHandle);

		/* Check if volume is on current partition */

		if (volumeExtentBuffer->NumberOfDiskExtents <= 1) {
			if (volumeExtentBuffer->Extents[0].DiskNumber == disk->getDiskNumber())
			{
				if (volumeExtentBuffer->Extents[0].StartingOffset.QuadPart > this->partitionInformation.StartingOffset.QuadPart && volumeExtentBuffer->Extents[0].StartingOffset.QuadPart < this->partitionInformation.StartingOffset.QuadPart + this->partitionInformation.PartitionLength.QuadPart)
				{
					if (mountedVoumes == NULL)
					{
						LOG_ERROR(L"%s", "NULL function argument: mountedDevcie!");
						free(volumeExtentBuffer);
						return TRUE;
					}
					else {
						wcscpy(mountedVoumes, drivePath);
						free(volumeExtentBuffer);
						return TRUE;
					}
				}
			}
			else {
				continue;
			}
		}
		else {
			LOG_ERROR(L"%s", "NUMBER OF EXTENTS > 1, NOT SUPPORTED YET!");
		}
	}

	free(volumeExtentBuffer);
	return FALSE;
}

bool CPartition::erase(OverwritePattern pattern)
{
	unmountPartitionVolumes();
	BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);
	do
	{
		LARGE_INTEGER offset = partitionInformation.StartingOffset;
		LARGE_INTEGER lenght = partitionInformation.PartitionLength;
		DWORD bytesWritten;
		unsigned char buffer[512];

		while (lenght.QuadPart > 0)
		{
			patternGen->getBuffer(buffer, 512);
			bytesWritten = disk->write(offset, buffer, 512);
			if (bytesWritten == 0)
			{
				LOG_ERROR(L"%s", ShowError());
				return false;
			}
			lenght.QuadPart -= 512;
		}
	} while (patternGen->nextPattern() != -1);
	delete patternGen;
	return -1;
}

bool CPartition::forceErase(OverwritePattern pattern)
{

	unmountPartitionVolumes();
	BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);
	do
	{
		LARGE_INTEGER offset;
		offset.QuadPart= partitionInformation.StartingOffset.QuadPart / 512;
		LARGE_INTEGER lenght = partitionInformation.PartitionLength;
		DWORD bytesWritten;
		unsigned char buffer[512];

		while (lenght.QuadPart > 0)
		{
			patternGen->getBuffer(buffer, 512);
			CDriverAccess::write_sector(buffer, disk->getDiskNumber(), offset);
			offset.QuadPart++;
			lenght.QuadPart -= 512;
		}
	} while (patternGen->nextPattern() != -1);

	delete patternGen;
	

	return true;

}