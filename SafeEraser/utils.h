#pragma once

#include <stdio.h>
#include <ctype.h>

/* !!! Winsock2 should precede every windows.h include*/
#include <WinSock2.h>
#include <Windows.h>

#define WITH_LOG_INFO

#define EMPTY_ARGS(...) (#__VA_ARGS__[0] == '\0' ? 1 : 0)

#define LOG_ERROR(str, ...) fwprintf(stderr, L"ERROR: %s: %s: %d: " str "\n", __FILEW__, \
							  __FUNCTIONW__, __LINE__, __VA_ARGS__)

#ifdef WITH_LOG_INFO
#define LOG_INFO(str, ...) fwprintf(stderr, L"LOG: %s: %s: %d: " str "\n", __FILEW__, \
							  __FUNCTIONW__, __LINE__, __VA_ARGS__)
#else
#define LOG_INFO(fstr, ...)
#endif

#define BYTES_TO_KB (1024)
#define BYTES_TO_MB (BYTES_TO_KB * 1024)
#define BYTES_TO_GB (BYTES_TO_MB * 1024)

static void inline hex_dump(void *buffer, int size) {
	unsigned char *pc = (unsigned char *)buffer;
	printf("      ");
	for (int i = 0; i < 16; i++) {
		printf("%2X ", i);
	}
	for (int i = 0; i < size; i++) {
		if (i % 8 == 0) {
			printf(" ");
		}
		if (i % 16 == 0) {
			printf("   ");
			for (int j = i - 16; i != 0 && j < i; j++)
			{
				if (isprint(pc[j]))
					printf("%c", pc[j]);
				else
					printf(".");
			}
			printf("\n%4X: ", i);
		}
		printf("%02X ", pc[i]);
	}
}

static inline WCHAR *ShowError(void)
{
	/* :) */
	static WCHAR ret[128] = { 0 };

	WCHAR *msgBuffer;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		GetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&msgBuffer,
		0,
		NULL
	);
	memset(ret, 0, sizeof ret);
	wcscpy(ret, msgBuffer);
	LocalFree(msgBuffer);
	return (WCHAR *)ret;
}

/* kinda strtok */
WCHAR getvolume(WCHAR **ppath);

/* kinda strtok */
int getpathname(WCHAR **ppath, WCHAR *pathname);