#include "FileObject.h"
#include "Ntddvol.h"    // IOCTL_VOLUME_PHYSICAL_TO_LOGICAL
#include "utils.h"
#include "CDriverAccess.h"



int CFileObject::getPhysicalOffsetOnDisk(LARGE_INTEGER *res)
{

	INT iExtentsBufferSize = 1024;
	DWORD dwBytesReturned;
	DWORD dwSectorsPerCluster;
	DWORD dwBytesPerSector;
	DWORD dwNumberFreeClusters;
	DWORD dwTotalNumberClusters;
	DWORD dwVolumeSerialNumber;
	DWORD dwMaxFileNameLength;
	DWORD dwFileSystemFlags;
	DWORD dwClusterSizeInBytes;

	STARTING_VCN_INPUT_BUFFER StartingPointInputBuffer;
	LARGE_INTEGER FileOffstFromBegDisk;
	VOLUME_LOGICAL_OFFSET VolumeLogicalOffset;
	LARGE_INTEGER  PhysicalOffsetReturnValue;
	VOLUME_PHYSICAL_OFFSETS VolumePhysicalOffsets;

	WCHAR volume_name[16];
	wsprintf(volume_name, L"%c:\\", path.c_str()[0]);

	HANDLE hFile;

	hFile = CreateFile(
		path.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (INVALID_HANDLE_VALUE == hFile) {
		wprintf(ShowError());
		return -1;
	}

	// Buffer to hold the extents info
	PRETRIEVAL_POINTERS_BUFFER lpRetrievalPointersBuffer =
		(PRETRIEVAL_POINTERS_BUFFER)malloc(iExtentsBufferSize);

	//StartingVcn field is the virtual cluster number in file
	// It is not a file offset

	// FSCTL_GET_RETRIEVAL_POINTERS acquires a list of virtual clusters from the
	// file system driver that make up the file and the related set of
	// Logical Clusters that represent the volume storage for these Virtual
	// Clusters.  On a heavliy fragmented volume, the file may not necessarily
	// be stored in contiguous storage locations.  Thus, it would be advisable
	// to follow the mapping of virtual to logical clusters "chain" to find
	// the complete physical layout of the file.

	// We want to start at the first virtual cluster ZERO   
	StartingPointInputBuffer.StartingVcn.QuadPart = 0;
	if (!DeviceIoControl(
		hFile,
		FSCTL_GET_RETRIEVAL_POINTERS,
		&StartingPointInputBuffer,
		sizeof(STARTING_VCN_INPUT_BUFFER),
		lpRetrievalPointersBuffer,
		iExtentsBufferSize,
		&dwBytesReturned,
		NULL))
	{
		wprintf(L"%s\n", ShowError());
		return -1;
	}
	CloseHandle(hFile);

	if (!GetDiskFreeSpace(
		volume_name,
		&dwSectorsPerCluster,
		&dwBytesPerSector,
		&dwNumberFreeClusters,
		&dwTotalNumberClusters))
	{
		return -1;
	}
	dwClusterSizeInBytes = dwSectorsPerCluster * dwBytesPerSector;

	TCHAR szVolumeName[128];
	TCHAR szFileSystemName[32];

	if (!GetVolumeInformation(
		volume_name,
		szVolumeName,
		128,
		&dwVolumeSerialNumber,
		&dwMaxFileNameLength,
		&dwFileSystemFlags,
		szFileSystemName,
		32))
		return -1;

	//* MODIFY **/
	wsprintf(volume_name, L"\\\\.\\%c:", path.c_str()[0]);
	HANDLE volumeHandle = CreateFile(
		volume_name,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING,
		NULL);

	if (volumeHandle == INVALID_HANDLE_VALUE)
		return -1;

	else if (wcscmp(szFileSystemName, L"NTFS") == 0)
	{
		VolumeLogicalOffset.LogicalOffset = lpRetrievalPointersBuffer->Extents[0].Lcn.QuadPart * dwClusterSizeInBytes;
		if (!DeviceIoControl(
			volumeHandle,
			IOCTL_VOLUME_LOGICAL_TO_PHYSICAL,
			&VolumeLogicalOffset,
			sizeof(VOLUME_LOGICAL_OFFSET),
			&VolumePhysicalOffsets,
			sizeof(VOLUME_PHYSICAL_OFFSETS),
			&dwBytesReturned,
			NULL))
		{
			CloseHandle(volumeHandle);
			return -1;
		}
		CloseHandle(volumeHandle);
		PhysicalOffsetReturnValue.QuadPart = 0;
		PhysicalOffsetReturnValue.QuadPart += VolumePhysicalOffsets.PhysicalOffset[0].Offset;
		if (PhysicalOffsetReturnValue.QuadPart != -1)
		{
			wprintf(L"%s starts at 0x%x%x from beginning of the disk\n\n",
				path.c_str(),
				PhysicalOffsetReturnValue.HighPart,
				PhysicalOffsetReturnValue.LowPart);

		}
	}
	else
	{
		printf("%s File system NOT supported\n", szFileSystemName);
		return  -1;
	}

	*res = PhysicalOffsetReturnValue;

	return 0;
}


bool CFileObject::erase(OverwritePattern pattern)
{
	HANDLE fHandle;
	DWORD bytesWritten;

	fHandle = CreateFile(
		path.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING,
		NULL);

	if (fHandle == INVALID_HANDLE_VALUE)
	{
		LOG_ERROR(L"%s", ShowError());
		return false;
	}

	{
		unsigned char buffer[512];
		BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);
		LARGE_INTEGER fSize;
		LARGE_INTEGER currentOffset;

		currentOffset.QuadPart = 0;

		GetFileSizeEx(fHandle, &fSize);
		do
		{
			/* Set cursor at beginning of file */
			SetFilePointer(fHandle, 0, NULL, FILE_BEGIN);
			currentOffset.QuadPart = 0;

			while (currentOffset.QuadPart < fSize.QuadPart)
			{
				patternGen->getBuffer(buffer, 512);
				WriteFile(fHandle, buffer, 512, &bytesWritten, NULL);
				if (bytesWritten == 0)
				{
					LOG_ERROR(L"%s", ShowError());
					return false;
				}
				currentOffset.QuadPart += bytesWritten;
			}
		} while (patternGen->nextPattern() != -1);

		delete patternGen;
	}


	CloseHandle(fHandle);
	_wremove(path.c_str());
	return true;
}


/* Transform it to erase: */
bool CFileObject::forceErase(OverwritePattern pattern)
{
	DWORD bytesWritten;
	DataRun_Entry *dr_entry;
	unsigned char buffer[512];
	BaseGenerator* patternGen = GeneratorFactory::getPatternGenerator(pattern);

	ULONGLONG MFToffset = volume->GetVolumeDiskOffset() + volume->GetMFTAddr() + (volume->GetFileRecordSize()) * fileRecord->GetFileRefference();
	printf("MFT entry at: %llu\n", MFToffset);
	const CAttrBase* attr = fileRecord->FindStream();
	if (attr && attr->IsNonResident())
	{
		const CDataRunList &dr = ((CAttrNonResident *)fileRecord->FindStream())->getDataRunList();
		dr_entry = dr.FindFirstEntry();
		while (dr_entry) {
			ULONGLONG start = volume->GetVolumeDiskOffset() + volume->GetClusterSize() * (dr_entry->LCN + dr_entry->StartVCN);
			ULONGLONG stop = volume->GetVolumeDiskOffset() + volume->GetClusterSize() *  (dr_entry->LCN + dr_entry->LastVCN + 1);

			LOG_INFO(L"Data run clusters: %llu - %llu", start, stop);

			/* Overwrite data */
			LARGE_INTEGER current;
			current.QuadPart = start / volume->GetSectorSize();
			do
			{
				for (uint64_t i = start; i < stop; i += 512)
				{
					patternGen->getBuffer(buffer, 512);
					CDriverAccess::write_sector(buffer, this->physicalDevice, current);
					current.QuadPart++;
				}
			} while (patternGen->nextPattern() != -1);
			dr_entry = dr.FindNextEntry();
		}

	}

	/* Clear filesystem buffer*/
	HANDLE fHandle = CreateFile(
		path.c_str(),
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING,
		NULL);
	CloseHandle(fHandle);

	/* Delete  file */
	if (fileRecord->IsDirectory())
		RemoveDirectory(path.c_str());
	else
		_wremove(path.c_str());

	/* Overwrite MFT */
	patternGen->reset();
	LARGE_INTEGER MFToffsteLI;

	do {
		MFToffsteLI.QuadPart = MFToffset / volume->GetSectorSize();
		for (int i = 0; i < volume->GetFileRecordSize(); i += 512)
		{
			patternGen->getBuffer(buffer, 512);
			CDriverAccess::write_sector(buffer, this->physicalDevice, MFToffsteLI);
			MFToffsteLI.QuadPart += 1;
		}
	} while (patternGen->nextPattern() != -1);

	MFToffsteLI.QuadPart = MFToffset / volume->GetSectorSize();
	/* Setting to 0 to change flags so the FS interpret entry as free*/
	memset(buffer, 0,  512);
	CDriverAccess::write_sector(buffer, this->physicalDevice, MFToffsteLI);
	delete patternGen;

	return true;
}

CFileObject::CFileObject(wstring aPath) : path(aPath)
{
	WCHAR *lPath = wcsdup(aPath.c_str());
	WCHAR* lPathInitPointer = lPath;
	WCHAR volumeLetter = getvolume(&lPath);

	if (isalpha(volumeLetter))
	{
		volume = new CNTFSVolume(volumeLetter);
	}
	else {
		LOG_ERROR(L"%s", L"Invalid File Name");
		return;
	}

	fileRecord = new CFileRecord(volume);
	fileRecord->SetAttrMask(MASK_DATA | MASK_INDEX_ROOT | MASK_INDEX_ALLOCATION);

	if (!fileRecord->ParseFileRecord(MFT_IDX_ROOT))
	{
		printf("Cannot read root directory of volume %c\n", volumeLetter);
		return;
	}

	if (!fileRecord->ParseAttrs())
	{
		printf("Cannot parse attributes\n");
		return;
	}

	WCHAR pathname[MAX_PATH];
	int pathlen;


	/* Parse NTFS tree to obtain file record */
	while (1)
	{
		pathlen = getpathname(&lPath, pathname);
		if (pathlen < 0)	// parameter syntax error
		{
			return;
		}
		if (pathlen == 0)
			break;	// no subdirectories

		CIndexEntry ie;
		if (fileRecord->FindSubEntry(pathname, ie))
		{
			if (ie.IsDirectory())
			{
				if (!fileRecord->ParseFileRecord(ie.GetFileReference()))
				{
					printf("Cannot read directory %s\n", pathname);
					return;
				}
				if (!fileRecord->ParseAttrs())
				{
					printf("Cannot parse attributes\n");
					return;
				}
			}
			else
			{
				fileRecord->ParseFileRecord(ie.GetFileReference());
				if (!fileRecord->ParseAttrs())
				{
					printf("Cannot parse attributes\n");
					return;
				}
				break;
			}
		}
		else
		{
			wprintf(L"Cannot find directory %s\n", pathname);
			return;
		}
	}
	/*
	if (fileRecord->FindStream()->IsNonResident())
		((CAttrNonResident*)fileRecord->FindStream())
*/
/* TODO: Modify this to use NTFSVolume class maybe?*/
	{
		WCHAR volumePath[16];
		DWORD volumeExtentsBufferSize = sizeof(VOLUME_DISK_EXTENTS) + (4 * sizeof(DISK_EXTENT));
		PVOLUME_DISK_EXTENTS volumeExtentBuffer = (VOLUME_DISK_EXTENTS*)malloc(volumeExtentsBufferSize);

		wsprintf(volumePath, L"\\\\.\\%c:", volumeLetter);

		HANDLE CHandle = CreateFile(
			volumePath,
			GENERIC_READ | GENERIC_WRITE,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);


		if (!DeviceIoControl((HANDLE)CHandle,     // handle to device
			IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS, // dwIoControlCode
			NULL,                                 // lpInBuffer
			0,                                    // nInBufferSize
			volumeExtentBuffer,                   // output buffer
			volumeExtentsBufferSize,              // size of output buffer
			NULL,							   	  // number of bytes returned
			NULL								  // OVERLAPPED structure
		))
		{
			CloseHandle(CHandle);
			LOG_ERROR("%s", ShowError());
			return;
		}
		CloseHandle(CHandle);

		this->physicalDevice = volumeExtentBuffer->Extents[0].DiskNumber;
	}
	free(lPathInitPointer);
}

CFileObject::~CFileObject()
{
	if (volume != NULL)
		delete volume;
	if (fileRecord != NULL)
		delete fileRecord;
}

