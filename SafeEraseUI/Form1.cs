﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        private ServiceClient sc;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sc = new ServiceClient("127.0.0.1", 13579);

            byte[] bufferOut;
            byte[] bufferIn = new byte[2048];

            int outLen = 0;
            int inLen = 0;

            /* GET DISKS */
            bufferOut = Utils.ConvertIntToByteArray(4);
            sc.Send(bufferOut, 4);

            bufferOut = Utils.ConvertIntToByteArray((int)FunctionId.GET_DISKS);
            sc.Send(bufferOut, 4);
            sc.Receive(bufferIn, 4);

            inLen = Utils.ConvertBytesToInt(bufferIn);

            sc.Receive(bufferIn, inLen);

            String phd = Utils.ConvertByteToString(bufferIn, 0, inLen);


            char[] delim = new char[1];
            delim[0] = '\0';
            String[] str1 = phd.Split(delim, StringSplitOptions.RemoveEmptyEntries);

            CB_diskErase_selectDisk.Items.AddRange(str1);

            /* END GET DISK */

            foreach (string op in Enum.GetNames(typeof(OverwritePattern)))
            {
                CB_fileErase_overwritePattern.Items.Add(op);
                CB_partitionErase_overwritePattern.Items.Add(op);
                CB_diskErase_overwritePattern.Items.Add(op);
                CB_taskSched_model.Items.Add(op);
            }


            /* GET PARTITIONS */

            bufferOut = Utils.ConvertIntToByteArray(4);
            sc.Send(bufferOut, 4);

            bufferOut = Utils.ConvertIntToByteArray((int)FunctionId.GET_PARTITIONS);
            sc.Send(bufferOut, 4);
            sc.Receive(bufferIn, 4);

            inLen = Utils.ConvertBytesToInt(bufferIn);
            sc.Receive(bufferIn, inLen);
            phd = Utils.ConvertByteToString(bufferIn, 0, inLen);


            delim = new char[1];
            delim[0] = '\0';
            str1 = phd.Split(delim, StringSplitOptions.RemoveEmptyEntries);


            CB_partErase_select.Items.AddRange(str1);
            /* END GET PARTITIONS */

        }

        private void browseFileDirectTaskBt_Click(object sender, EventArgs e)
        {
            DialogResult result = OFD_fileErase_browse.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                TB_eraseFile_fileToDelete.Text = OFD_fileErase_browse.FileName;
            }
        }

        private void BT_fileErase_erase_Click(object sender, EventArgs e)
        {

            int outLen = OFD_fileErase_browse.FileName.Length + 1;
            /* because unicode */
            outLen *= 2;

            /* func ID + overwrite pattern */
            outLen += (4 + 4);

            sc.Send(Utils.ConvertIntToByteArray(outLen), 4);

            OverwritePattern op = (OverwritePattern)Enum.Parse(typeof(OverwritePattern),
                CB_fileErase_overwritePattern.Text, true);

            sc.Send(Utils.ConvertIntToByteArray((int)FunctionId.ERASE_FILE), 4);
            sc.Send(Utils.ConvertIntToByteArray((int)op), 4);

            sc.Send(Utils.ConvertStringToBytes(OFD_fileErase_browse.FileName + "\0"), outLen - 8);

        }

        private void browseFileSchedTaskBt_Click(object sender, EventArgs e)
        {
            DialogResult result = FBD_taskSched_browse.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                TB_taskSched_fileToDelete.Text = FBD_taskSched_browse.SelectedPath;
            }
        }

        private void BT_taskShedule_Schedule_Click(object sender, EventArgs e)
        {
            StringBuilder task_string = new StringBuilder();



            byte[] bufferOut;
            byte[] bufferIn = new byte[2048];
            byte[] rectype;

            int outLen = 0;
            int inLen = 0;

            if (CB_taskSched_model.Text.Length < 1)
            {
                MessageBox.Show("Alegeti tipul de suprascrere!");
                return;
            }

            if (TB_taskSched_fileToDelete.Text.Length < 1)
            {
                MessageBox.Show("Alegeti folderul!");
                return;
            }
            OverwritePattern op = (OverwritePattern)Enum.Parse(typeof(OverwritePattern),
               CB_taskSched_model.Text, true);

            if (RB_taskSchedule_oneTime.Checked == true)
            {

                task_string.Append(DTP_taskSched_date.Text);
                task_string.Append(" ");
                task_string.Append(DTP_taskSched_time.Text);

                bufferOut = Utils.ConvertIntToByteArray(46 + TB_taskSched_fileToDelete.Text.Length * 2);
                sc.Send(bufferOut, 4);

                bufferOut = Utils.ConvertIntToByteArray((int)FunctionId.SCHED_TASK_NONREC);
                sc.Send(bufferOut, 4);

                sc.Send(Utils.ConvertStringToBytes(task_string.ToString()), 38);

                sc.Send(Utils.ConvertIntToByteArray((int)op), 4);

                sc.Send(Utils.ConvertStringToBytes(TB_taskSched_fileToDelete.Text), TB_taskSched_fileToDelete.Text.Length * 2);

                MessageBox.Show("Task scheduled at: " + task_string.ToString(), "Success!");
            }
            else if (RB_taskSchedulle_recursive.Checked == true)
            {


                if (RB_taskSchedule_day.Checked == true)
                    rectype = Utils.ConvertIntToByteArray((int)Schedulling.SCHED_DAILY);
                else if (RB_taskSchedule_week.Checked == true)
                    rectype = Utils.ConvertIntToByteArray((int)Schedulling.SCHED_WEEKLY);
                else if (RB_taskSchedule_month.Checked == true)
                    rectype = Utils.ConvertIntToByteArray((int)Schedulling.SCHED_WEEKLY);
                else
                {
                    MessageBox.Show("Va rugam alegeti tipul de recursivitate!");
                    return;
                }

                int hour = DTP_schedTask_timeRec.Value.Hour;
                int min = DTP_schedTask_timeRec.Value.Minute;

                bufferOut = Utils.ConvertIntToByteArray(20 + TB_taskSched_fileToDelete.Text.Length * 2);
                sc.Send(bufferOut, 4);

                bufferOut = Utils.ConvertIntToByteArray((int)FunctionId.SCHED_TASK_REC);
                sc.Send(bufferOut, 4);

                sc.Send(rectype, 4);

                bufferOut = Utils.ConvertIntToByteArray(hour);
                sc.Send(bufferOut, 4);

                bufferOut = Utils.ConvertIntToByteArray(min);
                sc.Send(bufferOut, 4);

                bufferOut = Utils.ConvertIntToByteArray((int)op);
                sc.Send(bufferOut, 4);

                sc.Send(Utils.ConvertStringToBytes(TB_taskSched_fileToDelete.Text), TB_taskSched_fileToDelete.Text.Length * 2);

            }
            else
            {
                MessageBox.Show("Alegeti tipul de sarcina!");
            }
        }

        private void FBD_taskSched_browse_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int outlen = 0;
            /* func ID + overwrite pattern */
            outlen += 4 * 4;

            sc.Send(Utils.ConvertIntToByteArray(outlen), 4);

            OverwritePattern op = (OverwritePattern)Enum.Parse(typeof(OverwritePattern),
                CB_partitionErase_overwritePattern.Text, true);

            sc.Send(Utils.ConvertIntToByteArray((int)FunctionId.ERASE_PARTITIONS), 4);
            sc.Send(Utils.ConvertIntToByteArray((int)op), 4);


            string[] numbers = Regex.Split(CB_partErase_select.Text, @"\D+");
            int dev = int.Parse(numbers[1]);
            int part = int.Parse(numbers[2]);

            sc.Send(Utils.ConvertIntToByteArray(dev), 4);
            sc.Send(Utils.ConvertIntToByteArray(part), 4);

        }
    }
} 
