﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.fileErase = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BT_fileErase_erase = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CB_fileErase_overwritePattern = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TB_eraseFile_fileToDelete = new System.Windows.Forms.TextBox();
            this.browseFileDirectTaskBt = new System.Windows.Forms.Button();
            this.partitionErase = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.CB_partitionErase_overwritePattern = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CB_partErase_select = new System.Windows.Forms.ComboBox();
            this.diskErase = new System.Windows.Forms.TabPage();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.CB_diskErase_overwritePattern = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.CB_diskErase_selectDisk = new System.Windows.Forms.ComboBox();
            this.scheduledTask = new System.Windows.Forms.TabPage();
            this.BT_taskShedule_Schedule = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.CB_taskSched_model = new System.Windows.Forms.ComboBox();
            this.RB_taskSchedulle_recursive = new System.Windows.Forms.RadioButton();
            this.RB_taskSchedule_oneTime = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.DTP_schedTask_timeRec = new System.Windows.Forms.DateTimePicker();
            this.RB_taskSchedule_month = new System.Windows.Forms.RadioButton();
            this.RB_taskSchedule_week = new System.Windows.Forms.RadioButton();
            this.RB_taskSchedule_day = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.DTP_taskSched_date = new System.Windows.Forms.DateTimePicker();
            this.DTP_taskSched_time = new System.Windows.Forms.DateTimePicker();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.TB_taskSched_fileToDelete = new System.Windows.Forms.TextBox();
            this.browseFileSchedTaskBt = new System.Windows.Forms.Button();
            this.OFD_fileErase_browse = new System.Windows.Forms.OpenFileDialog();
            this.FBD_taskSched_browse = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.fileErase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.partitionErase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.diskErase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.scheduledTask.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.fileErase);
            this.tabControl1.Controls.Add(this.partitionErase);
            this.tabControl1.Controls.Add(this.diskErase);
            this.tabControl1.Controls.Add(this.scheduledTask);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(381, 475);
            this.tabControl1.TabIndex = 0;
            // 
            // fileErase
            // 
            this.fileErase.Controls.Add(this.pictureBox1);
            this.fileErase.Controls.Add(this.BT_fileErase_erase);
            this.fileErase.Controls.Add(this.groupBox2);
            this.fileErase.Controls.Add(this.groupBox1);
            this.fileErase.Location = new System.Drawing.Point(4, 22);
            this.fileErase.Name = "fileErase";
            this.fileErase.Padding = new System.Windows.Forms.Padding(3);
            this.fileErase.Size = new System.Drawing.Size(373, 449);
            this.fileErase.TabIndex = 0;
            this.fileErase.Text = "Fisier";
            this.fileErase.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 227);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(364, 216);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // BT_fileErase_erase
            // 
            this.BT_fileErase_erase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BT_fileErase_erase.Location = new System.Drawing.Point(292, 194);
            this.BT_fileErase_erase.Name = "BT_fileErase_erase";
            this.BT_fileErase_erase.Size = new System.Drawing.Size(75, 23);
            this.BT_fileErase_erase.TabIndex = 3;
            this.BT_fileErase_erase.Text = "Safe erase";
            this.BT_fileErase_erase.UseVisualStyleBackColor = true;
            this.BT_fileErase_erase.Click += new System.EventHandler(this.BT_fileErase_erase_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CB_fileErase_overwritePattern);
            this.groupBox2.Location = new System.Drawing.Point(9, 123);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(358, 65);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Alegeti modelul de suprascriere";
            // 
            // CB_fileErase_overwritePattern
            // 
            this.CB_fileErase_overwritePattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_fileErase_overwritePattern.FormattingEnabled = true;
            this.CB_fileErase_overwritePattern.Location = new System.Drawing.Point(6, 19);
            this.CB_fileErase_overwritePattern.Name = "CB_fileErase_overwritePattern";
            this.CB_fileErase_overwritePattern.Size = new System.Drawing.Size(187, 21);
            this.CB_fileErase_overwritePattern.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TB_eraseFile_fileToDelete);
            this.groupBox1.Controls.Add(this.browseFileDirectTaskBt);
            this.groupBox1.Location = new System.Drawing.Point(8, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(359, 85);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alegeti fisierul";
            // 
            // TB_eraseFile_fileToDelete
            // 
            this.TB_eraseFile_fileToDelete.AllowDrop = true;
            this.TB_eraseFile_fileToDelete.Location = new System.Drawing.Point(6, 18);
            this.TB_eraseFile_fileToDelete.Name = "TB_eraseFile_fileToDelete";
            this.TB_eraseFile_fileToDelete.ReadOnly = true;
            this.TB_eraseFile_fileToDelete.Size = new System.Drawing.Size(347, 20);
            this.TB_eraseFile_fileToDelete.TabIndex = 1;
            // 
            // browseFileDirectTaskBt
            // 
            this.browseFileDirectTaskBt.Location = new System.Drawing.Point(278, 44);
            this.browseFileDirectTaskBt.Name = "browseFileDirectTaskBt";
            this.browseFileDirectTaskBt.Size = new System.Drawing.Size(75, 23);
            this.browseFileDirectTaskBt.TabIndex = 0;
            this.browseFileDirectTaskBt.Text = "Browse";
            this.browseFileDirectTaskBt.UseVisualStyleBackColor = true;
            this.browseFileDirectTaskBt.Click += new System.EventHandler(this.browseFileDirectTaskBt_Click);
            // 
            // partitionErase
            // 
            this.partitionErase.Controls.Add(this.pictureBox2);
            this.partitionErase.Controls.Add(this.groupBox4);
            this.partitionErase.Controls.Add(this.button2);
            this.partitionErase.Controls.Add(this.groupBox3);
            this.partitionErase.Location = new System.Drawing.Point(4, 22);
            this.partitionErase.Name = "partitionErase";
            this.partitionErase.Padding = new System.Windows.Forms.Padding(3);
            this.partitionErase.Size = new System.Drawing.Size(373, 449);
            this.partitionErase.TabIndex = 2;
            this.partitionErase.Text = "Partitie";
            this.partitionErase.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImageLocation = "";
            this.pictureBox2.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.InitialImage")));
            this.pictureBox2.Location = new System.Drawing.Point(6, 223);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(361, 216);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CB_partitionErase_overwritePattern);
            this.groupBox4.Location = new System.Drawing.Point(6, 120);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(361, 68);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Alegere model de suprascriere";
            // 
            // CB_partitionErase_overwritePattern
            // 
            this.CB_partitionErase_overwritePattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_partitionErase_overwritePattern.FormattingEnabled = true;
            this.CB_partitionErase_overwritePattern.Location = new System.Drawing.Point(6, 19);
            this.CB_partitionErase_overwritePattern.Name = "CB_partitionErase_overwritePattern";
            this.CB_partitionErase_overwritePattern.Size = new System.Drawing.Size(187, 21);
            this.CB_partitionErase_overwritePattern.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(292, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Safe erase";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CB_partErase_select);
            this.groupBox3.Location = new System.Drawing.Point(6, 31);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(361, 83);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Selectare Partitie";
            // 
            // CB_partErase_select
            // 
            this.CB_partErase_select.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_partErase_select.FormattingEnabled = true;
            this.CB_partErase_select.Location = new System.Drawing.Point(6, 19);
            this.CB_partErase_select.Name = "CB_partErase_select";
            this.CB_partErase_select.Size = new System.Drawing.Size(349, 21);
            this.CB_partErase_select.TabIndex = 0;
            // 
            // diskErase
            // 
            this.diskErase.Controls.Add(this.pictureBox3);
            this.diskErase.Controls.Add(this.groupBox5);
            this.diskErase.Controls.Add(this.button3);
            this.diskErase.Controls.Add(this.groupBox6);
            this.diskErase.Location = new System.Drawing.Point(4, 22);
            this.diskErase.Name = "diskErase";
            this.diskErase.Size = new System.Drawing.Size(373, 449);
            this.diskErase.TabIndex = 3;
            this.diskErase.Text = "Disc";
            this.diskErase.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.ImageLocation = "";
            this.pictureBox3.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.InitialImage")));
            this.pictureBox3.Location = new System.Drawing.Point(7, 230);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(356, 205);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 9;
            this.pictureBox3.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.CB_diskErase_overwritePattern);
            this.groupBox5.Location = new System.Drawing.Point(7, 123);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(363, 72);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Alegere model de suprascriere";
            // 
            // CB_diskErase_overwritePattern
            // 
            this.CB_diskErase_overwritePattern.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_diskErase_overwritePattern.FormattingEnabled = true;
            this.CB_diskErase_overwritePattern.Location = new System.Drawing.Point(6, 19);
            this.CB_diskErase_overwritePattern.Name = "CB_diskErase_overwritePattern";
            this.CB_diskErase_overwritePattern.Size = new System.Drawing.Size(187, 21);
            this.CB_diskErase_overwritePattern.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(289, 201);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "Safe erase";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.CB_diskErase_selectDisk);
            this.groupBox6.Location = new System.Drawing.Point(7, 32);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(363, 85);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Selectare disc";
            // 
            // CB_diskErase_selectDisk
            // 
            this.CB_diskErase_selectDisk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_diskErase_selectDisk.FormattingEnabled = true;
            this.CB_diskErase_selectDisk.Location = new System.Drawing.Point(6, 19);
            this.CB_diskErase_selectDisk.Name = "CB_diskErase_selectDisk";
            this.CB_diskErase_selectDisk.Size = new System.Drawing.Size(351, 21);
            this.CB_diskErase_selectDisk.TabIndex = 0;
            // 
            // scheduledTask
            // 
            this.scheduledTask.Controls.Add(this.BT_taskShedule_Schedule);
            this.scheduledTask.Controls.Add(this.groupBox8);
            this.scheduledTask.Controls.Add(this.groupBox7);
            this.scheduledTask.Location = new System.Drawing.Point(4, 22);
            this.scheduledTask.Name = "scheduledTask";
            this.scheduledTask.Padding = new System.Windows.Forms.Padding(3);
            this.scheduledTask.Size = new System.Drawing.Size(373, 449);
            this.scheduledTask.TabIndex = 1;
            this.scheduledTask.Text = "Planificare sarcina";
            this.scheduledTask.UseVisualStyleBackColor = true;
            // 
            // BT_taskShedule_Schedule
            // 
            this.BT_taskShedule_Schedule.Location = new System.Drawing.Point(292, 420);
            this.BT_taskShedule_Schedule.Name = "BT_taskShedule_Schedule";
            this.BT_taskShedule_Schedule.Size = new System.Drawing.Size(75, 23);
            this.BT_taskShedule_Schedule.TabIndex = 5;
            this.BT_taskShedule_Schedule.Text = "Schedule";
            this.BT_taskShedule_Schedule.UseVisualStyleBackColor = true;
            this.BT_taskShedule_Schedule.Click += new System.EventHandler(this.BT_taskShedule_Schedule_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Controls.Add(this.RB_taskSchedulle_recursive);
            this.groupBox8.Controls.Add(this.RB_taskSchedule_oneTime);
            this.groupBox8.Controls.Add(this.groupBox10);
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Location = new System.Drawing.Point(6, 91);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(361, 323);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Planificare";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.CB_taskSched_model);
            this.groupBox11.Location = new System.Drawing.Point(6, 211);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(349, 72);
            this.groupBox11.TabIndex = 9;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Alegere model de suprascriere";
            // 
            // CB_taskSched_model
            // 
            this.CB_taskSched_model.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_taskSched_model.FormattingEnabled = true;
            this.CB_taskSched_model.Location = new System.Drawing.Point(6, 19);
            this.CB_taskSched_model.Name = "CB_taskSched_model";
            this.CB_taskSched_model.Size = new System.Drawing.Size(187, 21);
            this.CB_taskSched_model.TabIndex = 3;
            // 
            // RB_taskSchedulle_recursive
            // 
            this.RB_taskSchedulle_recursive.AutoSize = true;
            this.RB_taskSchedulle_recursive.Location = new System.Drawing.Point(12, 99);
            this.RB_taskSchedulle_recursive.Name = "RB_taskSchedulle_recursive";
            this.RB_taskSchedulle_recursive.Size = new System.Drawing.Size(67, 17);
            this.RB_taskSchedulle_recursive.TabIndex = 6;
            this.RB_taskSchedulle_recursive.TabStop = true;
            this.RB_taskSchedulle_recursive.Text = "Recursiv";
            this.RB_taskSchedulle_recursive.UseVisualStyleBackColor = true;
            // 
            // RB_taskSchedule_oneTime
            // 
            this.RB_taskSchedule_oneTime.AutoSize = true;
            this.RB_taskSchedule_oneTime.Location = new System.Drawing.Point(12, 19);
            this.RB_taskSchedule_oneTime.Name = "RB_taskSchedule_oneTime";
            this.RB_taskSchedule_oneTime.Size = new System.Drawing.Size(94, 17);
            this.RB_taskSchedule_oneTime.TabIndex = 5;
            this.RB_taskSchedule_oneTime.TabStop = true;
            this.RB_taskSchedule_oneTime.Text = "O singura data";
            this.RB_taskSchedule_oneTime.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.DTP_schedTask_timeRec);
            this.groupBox10.Controls.Add(this.RB_taskSchedule_month);
            this.groupBox10.Controls.Add(this.RB_taskSchedule_week);
            this.groupBox10.Controls.Add(this.RB_taskSchedule_day);
            this.groupBox10.Controls.Add(this.label4);
            this.groupBox10.Controls.Add(this.label3);
            this.groupBox10.Location = new System.Drawing.Point(6, 122);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(349, 83);
            this.groupBox10.TabIndex = 4;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Recursiv";
            // 
            // DTP_schedTask_timeRec
            // 
            this.DTP_schedTask_timeRec.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.DTP_schedTask_timeRec.Location = new System.Drawing.Point(53, 49);
            this.DTP_schedTask_timeRec.Name = "DTP_schedTask_timeRec";
            this.DTP_schedTask_timeRec.ShowUpDown = true;
            this.DTP_schedTask_timeRec.Size = new System.Drawing.Size(91, 20);
            this.DTP_schedTask_timeRec.TabIndex = 3;
            this.DTP_schedTask_timeRec.UseWaitCursor = true;
            // 
            // RB_taskSchedule_month
            // 
            this.RB_taskSchedule_month.AutoSize = true;
            this.RB_taskSchedule_month.Location = new System.Drawing.Point(192, 20);
            this.RB_taskSchedule_month.Name = "RB_taskSchedule_month";
            this.RB_taskSchedule_month.Size = new System.Drawing.Size(49, 17);
            this.RB_taskSchedule_month.TabIndex = 4;
            this.RB_taskSchedule_month.TabStop = true;
            this.RB_taskSchedule_month.Text = "Luna";
            this.RB_taskSchedule_month.UseVisualStyleBackColor = true;
            // 
            // RB_taskSchedule_week
            // 
            this.RB_taskSchedule_week.AutoSize = true;
            this.RB_taskSchedule_week.Location = new System.Drawing.Point(107, 20);
            this.RB_taskSchedule_week.Name = "RB_taskSchedule_week";
            this.RB_taskSchedule_week.Size = new System.Drawing.Size(79, 17);
            this.RB_taskSchedule_week.TabIndex = 3;
            this.RB_taskSchedule_week.TabStop = true;
            this.RB_taskSchedule_week.Text = "Saptamana";
            this.RB_taskSchedule_week.UseVisualStyleBackColor = true;
            // 
            // RB_taskSchedule_day
            // 
            this.RB_taskSchedule_day.AutoSize = true;
            this.RB_taskSchedule_day.Location = new System.Drawing.Point(67, 20);
            this.RB_taskSchedule_day.Name = "RB_taskSchedule_day";
            this.RB_taskSchedule_day.Size = new System.Drawing.Size(34, 17);
            this.RB_taskSchedule_day.TabIndex = 2;
            this.RB_taskSchedule_day.TabStop = true;
            this.RB_taskSchedule_day.Text = "Zi";
            this.RB_taskSchedule_day.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "La ora:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "In fiecare:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.DTP_taskSched_date);
            this.groupBox9.Controls.Add(this.DTP_taskSched_time);
            this.groupBox9.Location = new System.Drawing.Point(6, 42);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(349, 51);
            this.groupBox9.TabIndex = 3;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "O singura data";
            // 
            // DTP_taskSched_date
            // 
            this.DTP_taskSched_date.CustomFormat = "yyyy-MM-dd";
            this.DTP_taskSched_date.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_taskSched_date.Location = new System.Drawing.Point(6, 19);
            this.DTP_taskSched_date.Name = "DTP_taskSched_date";
            this.DTP_taskSched_date.Size = new System.Drawing.Size(89, 20);
            this.DTP_taskSched_date.TabIndex = 1;
            // 
            // DTP_taskSched_time
            // 
            this.DTP_taskSched_time.CustomFormat = "HH:mm:ss";
            this.DTP_taskSched_time.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DTP_taskSched_time.Location = new System.Drawing.Point(101, 19);
            this.DTP_taskSched_time.Name = "DTP_taskSched_time";
            this.DTP_taskSched_time.ShowUpDown = true;
            this.DTP_taskSched_time.Size = new System.Drawing.Size(70, 20);
            this.DTP_taskSched_time.TabIndex = 2;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.TB_taskSched_fileToDelete);
            this.groupBox7.Controls.Add(this.browseFileSchedTaskBt);
            this.groupBox7.Location = new System.Drawing.Point(6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(361, 79);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Selecteaza folder";
            // 
            // TB_taskSched_fileToDelete
            // 
            this.TB_taskSched_fileToDelete.AllowDrop = true;
            this.TB_taskSched_fileToDelete.Location = new System.Drawing.Point(6, 18);
            this.TB_taskSched_fileToDelete.Name = "TB_taskSched_fileToDelete";
            this.TB_taskSched_fileToDelete.ReadOnly = true;
            this.TB_taskSched_fileToDelete.Size = new System.Drawing.Size(349, 20);
            this.TB_taskSched_fileToDelete.TabIndex = 1;
            // 
            // browseFileSchedTaskBt
            // 
            this.browseFileSchedTaskBt.Location = new System.Drawing.Point(280, 44);
            this.browseFileSchedTaskBt.Name = "browseFileSchedTaskBt";
            this.browseFileSchedTaskBt.Size = new System.Drawing.Size(75, 23);
            this.browseFileSchedTaskBt.TabIndex = 0;
            this.browseFileSchedTaskBt.Text = "Browse";
            this.browseFileSchedTaskBt.UseVisualStyleBackColor = true;
            this.browseFileSchedTaskBt.Click += new System.EventHandler(this.browseFileSchedTaskBt_Click);
            // 
            // FBD_taskSched_browse
            // 
            this.FBD_taskSched_browse.HelpRequest += new System.EventHandler(this.FBD_taskSched_browse_HelpRequest);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 499);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Safe Erase";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.fileErase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.partitionErase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.diskErase.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.scheduledTask.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage fileErase;
        private System.Windows.Forms.TabPage scheduledTask;
        private System.Windows.Forms.OpenFileDialog OFD_fileErase_browse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button browseFileDirectTaskBt;
        private System.Windows.Forms.TextBox TB_eraseFile_fileToDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox CB_fileErase_overwritePattern;
        private System.Windows.Forms.Button BT_fileErase_erase;
        private System.Windows.Forms.TabPage partitionErase;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox CB_partErase_select;
        private System.Windows.Forms.TabPage diskErase;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox CB_diskErase_selectDisk;
        private System.Windows.Forms.ComboBox CB_partitionErase_overwritePattern;
        private System.Windows.Forms.ComboBox CB_diskErase_overwritePattern;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button browseFileSchedTaskBt;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button BT_taskShedule_Schedule;
        private System.Windows.Forms.RadioButton RB_taskSchedulle_recursive;
        private System.Windows.Forms.RadioButton RB_taskSchedule_oneTime;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.DateTimePicker DTP_schedTask_timeRec;
        private System.Windows.Forms.RadioButton RB_taskSchedule_month;
        private System.Windows.Forms.RadioButton RB_taskSchedule_week;
        private System.Windows.Forms.RadioButton RB_taskSchedule_day;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DateTimePicker DTP_taskSched_date;
        private System.Windows.Forms.DateTimePicker DTP_taskSched_time;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox TB_taskSched_fileToDelete;
        private System.Windows.Forms.FolderBrowserDialog FBD_taskSched_browse;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ComboBox CB_taskSched_model;
    }
}

