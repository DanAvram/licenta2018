﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    enum FunctionId
    {
        GET_DISKS = 0,
        GET_PARTITIONS = 1,

        ERASE_FILE = 2,
        ERASE_FOLDER = 3,
        ERASE_PARTITIONS = 4,
        ERASE_DISK = 5,

        SCHED_TASK_REC = 6,
        SCHED_TASK_NONREC = 7,
    };

    enum OverwritePattern
    {

        SINGLE_ONE_OVERWRITE = 1,
        US_AIRFORCE_5020 = 2,
        SCHNEIER = 3,
        RCMP_TSSIT_OPS_II = 4,
    };


    enum Schedulling
    {
        SCHED_DAILY = 1,
        SCHED_WEEKLY = 2,
        SCHED_MONTHLY = 4,
    };
}
