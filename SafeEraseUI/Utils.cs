﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public static class Utils
    {
        public static byte[] ConvertIntToByteArray(int intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            //if (BitConverter.IsLittleEndian)
            //    Array.Reverse(intBytes);
            //byte[] result = intBytes;

            return intBytes;
        }
        public static byte[] ConvertIntToByteArray(int intValue, bool tolong)
        {
            Int64 val = intValue;
            return ConvertIntToByteArray(val);
        }
        public static byte[] ConvertIntToByteArray(Int64 intValue)
        {
            byte[] intBytes = BitConverter.GetBytes(intValue);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            //byte[] result = intBytes;
            return intBytes;
        }
        public static Int32 ConvertBytesToInt(byte[] intValue)
        {
            //Array.Reverse(intValue);
            return BitConverter.ToInt32(intValue, 0);
        }
        public static Int64 ConvertBytesToLong(byte[] data)
        {
            return BitConverter.ToInt64(data, 0);
        }
        public static byte[] ConvertStringToBytes(string strValue)
        {
            return Encoding.Unicode.GetBytes(strValue);
        }
        public static string ConvertByteToString(byte[] array, int offset, int length)
        {
            return System.Text.Encoding.Unicode.GetString(array, offset, length);
        }
    }

}
