﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class ServiceClient
    {
        private readonly Socket clientSocket;


        public ServiceClient(string ip, int port)
        {
            clientSocket = new Socket(SocketType.Stream, ProtocolType.Tcp);
            IPAddress ip_server = IPAddress.Parse(ip);
            IPEndPoint server = new IPEndPoint(ip_server, port);
            clientSocket.Connect(server);
        }

        ~ServiceClient()
        {
            clientSocket.Shutdown(SocketShutdown.Both);
        }
    

        public void Receive(byte[] buffer, int length)
        {
            clientSocket.Receive(buffer, length, SocketFlags.None);
        }

        public void Send(byte[] buffer, int length)
        {
            clientSocket.Send(buffer, length, SocketFlags.None);
        }
    }
}
