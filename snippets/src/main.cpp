#define UNICODE 1
#define _UNICODE 1

/* The code of interest is in the subroutine GetDriveGeometry. The
   code in main shows how to interpret the results of the call. */

#include <windows.h>
#include <winioctl.h>
#include <SetupAPI.h>
#include <devguid.h>
#include <stdio.h>

#pragma comment (lib, "Setupapi.lib")



#define wszDrive L"\\\\?\\scsi#disk&ven_nvme&prod_samsung_mzvlv256#5&3a6a197a&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}"

void DUMP(void *buffer, int size) {
	unsigned char *pc = (unsigned char *)buffer;
	printf("      ");
	for (int i = 0; i < 16; i++) {
		printf("%2X ", i);
	}
	do {
		for (int i = 0; i < size; i++) {
			if (i % 8 == 0) {
				printf(" ");
			}
			if (i % 16 == 0) {
				printf("\n%4X: ", i);
			}
			printf("%02X ", pc[i]);
		}
	} while (0);
}


BOOL PrintMBR(LPWSTR wszPath)
{

	HANDLE hFile = INVALID_HANDLE_VALUE;  // handle to the drive to be examined 
	OVERLAPPED ol = { 0 };
	hFile = CreateFile(wszPath,               // file to open
		GENERIC_READ,          // open for reading
		FILE_SHARE_READ,       // share for reading
		NULL,                  // default security
		OPEN_EXISTING,         // existing file only
		FILE_ATTRIBUTE_NORMAL, // normal file
		NULL);                 // no attr. template

	if (hFile == INVALID_HANDLE_VALUE)
	{
		printf("Mare eroare!\n");
		return -1;
	}
	SetFilePointer(hFile, 0, 0, FILE_BEGIN);

	char   ReadBuffer[513] = { 0 };
	DWORD read;
	BOOL ret = ReadFile(hFile, ReadBuffer, 512, &read, NULL);

	if (ret == 0)
	{
		printf("MBR Read failed!\n");
	}
	else {

		printf("MBR: \n");
		DUMP(ReadBuffer, 512);
		CloseHandle(hFile);
	}
	return 0;
}


BOOL GetDriveGeometry2(LPWSTR wszPath, DISK_GEOMETRY *pdg)
{
	HANDLE hDevice = INVALID_HANDLE_VALUE;  // handle to the drive to be examined 
	BOOL bResult = FALSE;                 // results flag
	DWORD junk = 0;                     // discard results

	hDevice = CreateFileW(wszPath,          // drive to open
		0,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,    // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDevice == INVALID_HANDLE_VALUE)    // cannot open the drive
	{
		return (FALSE);
	}

	bResult = DeviceIoControl(hDevice,                     // device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
		NULL, 0,                       // no input buffer
		pdg, sizeof(*pdg),            // output buffer
		&junk,                         // # bytes returned
		(LPOVERLAPPED)NULL);          // synchronous I/O

	CloseHandle(hDevice);

	return (bResult);
}

int printGeometry2()
{
	DISK_GEOMETRY pdg = { 0 }; // disk drive geometry structure
	BOOL bResult = FALSE;      // generic results flag
	ULONGLONG DiskSize = 0;    // size of the drive, in bytes

	bResult = GetDriveGeometry2(wszDrive, &pdg);

	if (bResult)
	{
		wprintf(L"Drive path      = %ws\n", wszDrive);
		wprintf(L"Cylinders       = %I64d\n", pdg.Cylinders);
		wprintf(L"Tracks/cylinder = %ld\n", (ULONG)pdg.TracksPerCylinder);
		wprintf(L"Sectors/track   = %ld\n", (ULONG)pdg.SectorsPerTrack);
		wprintf(L"Bytes/sector    = %ld\n", (ULONG)pdg.BytesPerSector);


		DiskSize = pdg.Cylinders.QuadPart * (ULONG)pdg.TracksPerCylinder *
			(ULONG)pdg.SectorsPerTrack * (ULONG)pdg.BytesPerSector;
		wprintf(L"Disk size       = %I64d (Bytes)\n"
			L"                = %.2f (Gb)\n",
			DiskSize, (double)DiskSize / (1024 * 1024 * 1024));
	}
	else

	{
		wprintf(L"GetDriveGeometry failed. Error %ld.\n", GetLastError());
	}
	PrintMBR(wszDrive);
	return ((int)bResult);
}

void DisplayVolumePaths(
	__in PWCHAR VolumeName
)
{
	DWORD  CharCount = MAX_PATH + 1;
	PWCHAR Names = NULL;
	PWCHAR NameIdx = NULL;
	BOOL   Success = FALSE;

	for (;;)
	{
		//
		//  Allocate a buffer to hold the paths.
		Names = (PWCHAR) new BYTE[CharCount * sizeof(WCHAR)];

		if (!Names)
		{
			//
			//  If memory can't be allocated, return.
			return;
		}

		//
		//  Obtain all of the paths
		//  for this volume.
		Success = GetVolumePathNamesForVolumeNameW(
			VolumeName, Names, CharCount, &CharCount
		);

		if (Success)
		{
			break;
		}

		if (GetLastError() != ERROR_MORE_DATA)
		{
			break;
		}

		//
		//  Try again with the
		//  new suggested size.
		delete[] Names;
		Names = NULL;
	}

	if (Success)
	{
		//
		//  Display the various paths.
		for (NameIdx = Names;
			NameIdx[0] != L'\0';
			NameIdx += wcslen(NameIdx) + 1)
		{
			wprintf(L"  %s", NameIdx);
		}
		wprintf(L"\n");
	}

	if (Names != NULL)
	{
		delete[] Names;
		Names = NULL;
	}

	return;
}

int printInfo()
{
	DWORD  CharCount = 0;
	WCHAR  DeviceName[MAX_PATH] = L"";
	DWORD  Error = ERROR_SUCCESS;
	HANDLE FindHandle = INVALID_HANDLE_VALUE;
	BOOL   Found = FALSE;
	size_t Index = 0;
	BOOL   Success = FALSE;
	WCHAR  VolumeName[MAX_PATH] = L"";

	//
	//  Enumerate all volumes in the system.
	FindHandle = FindFirstVolumeW(VolumeName, ARRAYSIZE(VolumeName));

	if (FindHandle == INVALID_HANDLE_VALUE)
	{
		Error = GetLastError();
		wprintf(L"FindFirstVolumeW failed with error code %d\n", Error);
		return 0;
	}

	for (;;)
	{
		//
		//  Skip the \\?\ prefix and remove the trailing backslash.
		Index = wcslen(VolumeName) - 1;

		if (VolumeName[0] != L'\\' ||
			VolumeName[1] != L'\\' ||
			VolumeName[2] != L'?' ||
			VolumeName[3] != L'\\' ||
			VolumeName[Index] != L'\\')
		{
			Error = ERROR_BAD_PATHNAME;
			wprintf(L"FindFirstVolumeW/FindNextVolumeW returned a bad path: %s\n", VolumeName);
			break;
		}

		//
		//  QueryDosDeviceW does not allow a trailing backslash,
		//  so temporarily remove it.
		VolumeName[Index] = L'\0';

		CharCount = QueryDosDeviceW(&VolumeName[4], DeviceName, ARRAYSIZE(DeviceName));

		VolumeName[Index] = L'\\';

		if (CharCount == 0)
		{
			Error = GetLastError();
			wprintf(L"QueryDosDeviceW failed with error code %d\n", Error);
			break;
		}

		wprintf(L"\nFound a device:\n %s", DeviceName);
		wprintf(L"\nVolume name: %s", VolumeName);
		wprintf(L"\nPaths:");
		DisplayVolumePaths(VolumeName);

		//
		//  Move on to the next volume.
		Success = FindNextVolumeW(FindHandle, VolumeName, ARRAYSIZE(VolumeName));

		if (!Success)
		{
			Error = GetLastError();

			if (Error != ERROR_NO_MORE_FILES)
			{
				wprintf(L"FindNextVolumeW failed with error code %d\n", Error);
				break;
			}

			//
			//  Finished iterating
			//  through all the volumes.
			Error = ERROR_SUCCESS;
			break;
		}
	}

	FindVolumeClose(FindHandle);
	FindHandle = INVALID_HANDLE_VALUE;

	return 0;
}

int ssmain()
{
	
	return 0;

	HDEVINFO DeviceInfoSet = SetupDiGetClassDevs(
                                    NULL,
                                    NULL,
                                    NULL,
                                    DIGCF_ALLCLASSES | DIGCF_PRESENT);
	SP_DEVINFO_DATA DeviceInfoData;
	SetupDiEnumDeviceInfo(DeviceInfoSet, 0, &DeviceInfoData);

	ZeroMemory(&DeviceInfoData, sizeof(SP_DEVINFO_DATA));
	DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
	int DeviceIndex = 0;
    
	while (SetupDiEnumDeviceInfo(
                             DeviceInfoSet,
                             DeviceIndex,
                             &DeviceInfoData)) {
    DeviceIndex++;

	DEVPROPKEY DEVPKEY_Device_Class;
	DEVPROPTYPE PropType;
	PBYTE DevGuid;
	DWORD Size = 0;

    if (!SetupDiGetDeviceProperty(
                                  DeviceInfoSet,
                                  &DeviceInfoData,
                                  &DEVPKEY_Device_Class,
                                  &PropType,
                                  (PBYTE)&DevGuid,
                                  sizeof(GUID),
                                  &Size,
                                  0) ) {

        DWORD Error = GetLastError();

        if (Error == ERROR_NOT_FOUND) {
            
			printf("This device has an unknown device setup class.\n");
            
            }
        }                 
    }

if (DeviceInfoSet) {
    SetupDiDestroyDeviceInfoList(DeviceInfoSet);
    }
	
	/*
	HANDLE handle;

	handle = SetupDiGetClassDevs(&GUID_DEVCLASS_DISKDRIVE, NULL, NULL, DIGCF_PRESENT);
	DISK_GEOMETRY pdg = { 0 };
	DWORD junk = 0;

	BOOL ret = DeviceIoControl(handle,                     // device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
		NULL, 0,                       // no input buffer
		&pdg, sizeof(pdg),            // output buffer
		&junk,                         // # bytes returned
		(LPOVERLAPPED)NULL);          // synchronous I/O
	if (ret == 0)
		printf("DeviceIoControl failed misserably!");
	else {
		wprintf(L"Drive path      = %ws\n", wszDrive);
		wprintf(L"Cylinders       = %I64d\n", pdg.Cylinders);
		wprintf(L"Tracks/cylinder = %ld\n", (ULONG)pdg.TracksPerCylinder);
		wprintf(L"Sectors/track   = %ld\n", (ULONG)pdg.SectorsPerTrack);
		wprintf(L"Bytes/sector    = %ld\n", (ULONG)pdg.BytesPerSector);
	}*/
	return 0;
}