#include <windows.h>
#include <iostream>
#include <winioctl.h>
#include <SetupAPI.h>
#include <devguid.h>
#include <stdio.h>

#include "utils.h"

using namespace std;


int printGeometry(LPWSTR wszDrive);
BOOL printDevices() {
	HDEVINFO diskDevices;
	SP_DEVICE_INTERFACE_DATA deviceInterfaceData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA deviceInterfaceDetailData;
	DWORD devNumber = 0;
	DWORD size;
	BOOL ret = FALSE;

	STORAGE_DEVICE_NUMBER diskNumber;
	DWORD bytesReturned;

	diskDevices = SetupDiGetClassDevs(
		&GUID_DEVINTERFACE_DISK,
		NULL,
		NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE
	);

	if (diskDevices == INVALID_HANDLE_VALUE) {
		LOG_ERROR("SetupDietClassDevs failed!");
		return false;
	}


	ZeroMemory(&deviceInterfaceData, sizeof(SP_DEVICE_INTERFACE_DATA));
	deviceInterfaceData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

	while (SetupDiEnumDeviceInterfaces(
		diskDevices,
		NULL,
		&GUID_DEVINTERFACE_DISK,
		devNumber,
		&deviceInterfaceData)) {

		devNumber++;

		// Returns details about a device interface in second argument
		// First time is called to get the size
		ret = SetupDiGetDeviceInterfaceDetail(diskDevices,
			&deviceInterfaceData,
			NULL,
			0,
			&size,
			NULL);
		if ((ret == FALSE) && (GetLastError() != ERROR_INSUFFICIENT_BUFFER)) {
			LOG_INFO("SetupDiGetDeviceInterfaceDetail failed!");
			continue;
		}

		deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(size);

		ZeroMemory(deviceInterfaceDetailData, size);
		deviceInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
		
		// Second time is called for the information
		ret = SetupDiGetDeviceInterfaceDetail(diskDevices,
			&deviceInterfaceData,
			deviceInterfaceDetailData,
			size,
			NULL,
			NULL);
		
		if (ret == FALSE) {
			LOG_INFO("SetupDiGetDeviceInterfaceDetail failed!");
			continue;
		}

		HANDLE disk = CreateFile(deviceInterfaceDetailData->DevicePath,
			GENERIC_READ,
			FILE_SHARE_READ | FILE_SHARE_WRITE,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (INVALID_HANDLE_VALUE == disk) {
			LOG_ERROR("Failed to CreateFile!");
			continue;
		}
	
		ret = DeviceIoControl(disk,
			IOCTL_STORAGE_GET_DEVICE_NUMBER,
			NULL,
			0,
			&diskNumber,
			sizeof(STORAGE_DEVICE_NUMBER),
			&bytesReturned,
			NULL);
		if (0 == ret) {
			LOG_ERROR("DeviceIoControl failed!");
			continue;
		}

		cout << deviceInterfaceDetailData->DevicePath << endl;
		cout << "\\\\.\\PhysicalDrive" << diskNumber.DeviceNumber << endl;

		LPWSTR devName =(LPWSTR) malloc(sizeof(LPWSTR) * 50);
		swprintf(devName, L"\\\\.\\PhysicalDrive%d", diskNumber.DeviceNumber);

		printGeometry(devName);
		free(devName);
	}
}


BOOL GetDriveGeometry(LPWSTR wszPath, DISK_GEOMETRY *pdg)
{
	HANDLE hDevice = INVALID_HANDLE_VALUE;
	BOOL bResult = FALSE;
	DWORD junk = 0;

	hDevice = CreateFileW(wszPath,
		0,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,    // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDevice == INVALID_HANDLE_VALUE)    // cannot open the drive
	{
		return (FALSE);
	}

	bResult = DeviceIoControl(hDevice,                     // device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
		NULL, 0,                       // no input buffer
		pdg, sizeof(*pdg),            // output buffer
		&junk,                         // # bytes returned
		(LPOVERLAPPED)NULL);          // synchronous I/O

	CloseHandle(hDevice);

	return (bResult);
}

int printGeometry(LPWSTR wszDrive)
{
	DISK_GEOMETRY pdg = { 0 }; // disk drive geometry structure
	BOOL bResult = FALSE;      // generic results flag
	ULONGLONG DiskSize = 0;    // size of the drive, in bytes

	bResult = GetDriveGeometry(wszDrive, &pdg);

	if (bResult)
	{
		wprintf(L"Drive path      = %ws\n", wszDrive);
		wprintf(L"Cylinders       = %I64d\n", pdg.Cylinders);
		wprintf(L"Tracks/cylinder = %ld\n", (ULONG)pdg.TracksPerCylinder);
		wprintf(L"Sectors/track   = %ld\n", (ULONG)pdg.SectorsPerTrack);
		wprintf(L"Bytes/sector    = %ld\n", (ULONG)pdg.BytesPerSector);


		DiskSize = pdg.Cylinders.QuadPart * (ULONG)pdg.TracksPerCylinder *
			(ULONG)pdg.SectorsPerTrack * (ULONG)pdg.BytesPerSector;
		wprintf(L"Disk size       = %I64d (Bytes)\n"
			L"                = %.2f (Gb)\n",
			DiskSize, (double)DiskSize / (1024 * 1024 * 1024));
	}
	else

	{
		wprintf(L"GetDriveGeometry failed. Error %ld.\n", GetLastError());
	}
	return ((int)bResult);
}

typedef struct tagFILE_RECORD_HEADER
{
	DWORD		Magic;			// "FILE"
	WORD		OffsetOfUS;		// Offset of Update Sequence
	WORD		SizeOfUS;		// Size in words of Update Sequence Number & Array
	ULONGLONG	LSN;			// $LogFile Sequence Number
	WORD		SeqNo;			// Sequence number
	WORD		Hardlinks;		// Hard link count
	WORD		OffsetOfAttr;	// Offset of the first Attribute
	WORD		Flags;			// Flags
	DWORD		RealSize;		// Real size of the FILE record
	DWORD		AllocSize;		// Allocated size of the FILE record
	ULONGLONG	RefToBase;		// File reference to the base FILE record
	WORD		NextAttrId;		// Next Attribute Id
	WORD		Align;			// Align to 4 byte boundary
	DWORD		RecordNo;		// Number of this MFT Record
} FILE_RECORD_HEADER;

#pragma pack(1)
typedef struct tagNTFS_BPB
{
	// jump instruction
	BYTE		Jmp[3];

	// signature
	BYTE		Signature[8];

	// BPB and extended BPB
	WORD		BytesPerSector;
	BYTE		SectorsPerCluster;
	WORD		ReservedSectors;
	BYTE		Zeros1[3];
	WORD		NotUsed1;
	BYTE		MediaDescriptor;
	WORD		Zeros2;
	WORD		SectorsPerTrack;
	WORD		NumberOfHeads;
	DWORD		HiddenSectors;
	DWORD		NotUsed2;
	DWORD		NotUsed3;
	ULONGLONG	TotalSectors;
	ULONGLONG	LCN_MFT;
	ULONGLONG	LCN_MFTMirr;
	DWORD		ClustersPerFileRecord;
	DWORD		ClustersPerIndexBlock;
	BYTE		VolumeSN[8];

	// boot code
	BYTE		Code[430];

	//0xAA55
	BYTE		_AA;
	BYTE		_55;
} NTFS_BPB;
#pragma pack()



#define	NTFS_SIGNATURE		"NTFS    "
HANDLE hVolume;
NTFS_BPB bpb;
LONGLONG MFTAddr;

// Open a volume ('a' - 'z', 'A' - 'Z'), get volume handle and BPB
BOOL OpenVolume(WCHAR volume)
{
	WCHAR volumePath[7];
	_snwprintf(volumePath, 6, (L"\\\\.\\%c:"), volume);
	volumePath[6] = (L'\0');

	hVolume = CreateFile(volumePath, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL, OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);
	if (hVolume != INVALID_HANDLE_VALUE)
	{
		DWORD num;
		

		// Read the first sector (boot sector)
		if (ReadFile(hVolume, &bpb, 512, &num, NULL) && num == 512)
		{
			if (strncmp((const char*)bpb.Signature, NTFS_SIGNATURE, 8) == 0)
			{
				// Log important volume parameters

				unsigned int SectorSize = bpb.BytesPerSector;
				wprintf(L"Sector Size = %u bytes\n", SectorSize);

				unsigned int ClusterSize = SectorSize * (UCHAR)bpb.SectorsPerCluster;
				wprintf(L"Cluster Size = %u bytes\n", ClusterSize);

				unsigned int FileRecordSize;
				int sz = (char)bpb.ClustersPerFileRecord;
				if (sz > 0)
					FileRecordSize = ClusterSize * sz;
				else
					FileRecordSize = 1 << (-sz);
				wprintf(L"FileRecord Size = %u bytes\n", FileRecordSize);

				unsigned int IndexBlockSize;
				sz = (char)bpb.ClustersPerIndexBlock;
				if (sz > 0)
					IndexBlockSize = ClusterSize * sz;
				else
					IndexBlockSize = 1 << (-sz);
				wprintf(L"IndexBlock Size = %u bytes\n", IndexBlockSize);

				MFTAddr = bpb.LCN_MFT * ClusterSize;
				wprintf(L"MFT address = 0x%016I64X\n", MFTAddr);
			}
			else
			{
				printf("Volume file system is not NTFS\n");
				goto IOError;
			}
		}
		else
		{
			printf("Read boot sector error\n");
			goto IOError;
		}
	}
	else
	{
		printf("Cannnot open volume %c\n", (char)volume);
	IOError:
		if (hVolume != INVALID_HANDLE_VALUE)
		{
			CloseHandle(hVolume);
			hVolume = INVALID_HANDLE_VALUE;
		}
		return FALSE;
	}

	return TRUE;
}


FILE_RECORD_HEADER *fr = NULL;
// Read File Record
FILE_RECORD_HEADER* ReadFileRecord(LONG low, LONG high)
{
	SetFilePointer(hVolume, low, &high, FILE_BEGIN);
	fr = (FILE_RECORD_HEADER*)new BYTE[1024];
	DWORD len;
	ReadFile(hVolume, fr, 1024, &len, NULL);
		return fr;
}

int main()
{
	OpenVolume(L'C');
	printf("MFT at: %u\n", bpb.LCN_MFT * bpb.SectorsPerCluster);

	char buffer[1024];

	printf("First file record:\n");

	LONG low = (LONG)MFTAddr;
	LONG high = (LONG)(MFTAddr>>32);

	SetFilePointer(hVolume, low + 0x444* 1024, &high, FILE_BEGIN);

	DWORD bread;
	ReadFile(hVolume, buffer, 1024, &bread, NULL);

	hex_dump(buffer, 1024);

/*/
	printDevices();
	
	HANDLE fileHandle;
	fileHandle = CreateFile(L"text.txt", GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	_BY_HANDLE_FILE_INFORMATION fileInfo;

	GetFileInformationByHandle(fileHandle, &fileInfo);

	wprintf(L"nFileIndexHigh = %ld, nFileIndexLow = %ld", fileInfo.nFileIndexHigh, fileInfo.nFileIndexLow);
	
	
	ReadFileRecord(fileInfo.nFileIndexLow, fileInfo.nFileIndexHigh);

//	printf("\nINFO:\n", fr.)
*/
	return 0;
}
