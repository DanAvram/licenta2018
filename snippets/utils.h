#pragma once

#include <stdio.h>

#define WITH_LOG_INFO


#define LOG_ERROR(str, ...) fprintf(stderr, "ERROR: %s: %s: %d: " str "\n", __FILE__, \
							  __func__, __LINE__, __VA_ARGS__);

#ifdef WITH_LOG_INFO
#define LOG_INFO(str, ...) fprintf(stderr, "LOG: %s: %s: %d: " str "\n", __FILE__, \
							  __func__, __LINE__, __VA_ARGS__);
#else
#define LOG_INFO(fstr, ...)
#endif

void inline hex_dump(void *buffer, int size) {
	unsigned char *pc = (unsigned char *)buffer;
	printf("      ");
	for (int i = 0; i < 16; i++) {
		printf("%2X ", i);
	}
	for (int i = 0; i < size; i++) {
		if (i % 8 == 0) {
			printf(" ");
		}
		if (i % 16 == 0) {
			printf("   ");
			for (int j = i - 16; i !=0 && j < i; j++)
			{
				if (isprint(pc[j]))
					printf("%c", pc[j]);
				else
					printf(".");
			}
			printf("\n%4X: ", i);
		}
		printf("%02X ", pc[i]);
	}
}